﻿
using EVT.TaskManager.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Services
{
    public class UserFactoryBase
    {
        #region Private Fields
        private readonly ILogger<UserFactoryBase> _logger;
        #endregion

        protected virtual UserBase DoCreateUser(string fullUserName, string email, string accountName, string? password = null)
        {
            UserBase user;
            if (password == null)
            {
                user = new UserBase(fullUserName, email, accountName);
            }
            else
            {
                user = new UserBase(fullUserName, email, accountName, password);
            }

            return user;
        }

        protected virtual void LogNewUserInfo(UserBase user)
        {
            _logger.LogInformation("New user was created: " + user.ToString());
        }

        #region FactoryMethod
        public UserBase CreateUser(string fullUserName, string email, string accountName = "", string? password = null)
        {
            var user = DoCreateUser(fullUserName, email, accountName, password);
            if (_logger != null) LogNewUserInfo(user);
            return user;
        }
        #endregion



        #region Ctor
        public UserFactoryBase(ILogger<UserFactoryBase> logger = null)
        {
            _logger = logger;
        }
        #endregion

    }
}
