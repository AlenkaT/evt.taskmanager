﻿
using EVT.TaskManager.Entities;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Services
{
    public class TaskFactoryBase
    {
        private readonly ILogger<TaskFactoryBase> _logger;
        protected virtual TaskBase DoCreateTask(string title, UserBase submitter, string? description, UserBase? handler, DateTime? deadlineDate)
        {
            var newTask= new TaskBase(title,  submitter, description, handler, deadlineDate);
            DoLogNewTaskInfo(newTask);
            return newTask; 
        }

        protected virtual void DoLogNewTaskInfo(TaskBase task)
        {
            if (_logger != null)
            {
                _logger.LogInformation("New task was created: " + task.ToString());
            }
        }

        public TaskBase CreateTask(string title, UserBase submitter, string? description, UserBase? handler, DateTime? deadlineDate)
        {
            var task= DoCreateTask(title, submitter, description, handler, deadlineDate);
            return task;
        }




        public TaskFactoryBase(ILogger<TaskFactoryBase> logger=null)
        {
            _logger = logger;
        }


    }
}
