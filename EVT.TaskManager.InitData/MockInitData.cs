﻿
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.InitData
{
    public class MockInitData
    {
        private static readonly MockInitData _instance;
        public static MockInitData Instance { get => _instance; }

        static MockInitData()
        {
            _instance = new MockInitData();
        }

        public IEnumerable<UserBase> Users { get; }
        public IEnumerable<UserBase> Admins { get; }
        public IEnumerable<TaskBase> Tasks { get; }


        //  private readonly UserFactoryBase _userFactory;
        //  private readonly TaskFactoryBase _taskFactory;

        protected MockInitData()
        {
            var userFactory = new UserFactoryBase();
            var taskFactory = new TaskFactoryBase();
            Users = new List<UserBase>()
                        {
                            userFactory.CreateUser("First Us1", "user1@mail.ru", "","123"),
                            userFactory.CreateUser("First Us2", "user2@mail.ru", "","123"),
                            userFactory.CreateUser("First Us3", "user3@mail.ru", "","123"),
                            userFactory.CreateUser("First Us4", "user4@mail.ru", "","123"),
                            userFactory.CreateUser("First Us5", "user5@mail.ru", "","123")
                        };
            Admins = new List<UserBase>()
                        {
                               userFactory.CreateUser("Admin", "admin@mail.ru","ADMIN","321")
                        };


            var user1 = Users.FirstOrDefault();
            var user2 = Users.LastOrDefault();
            Tasks = new List<TaskBase>()
                        {
                           taskFactory.CreateTask("Task01", user1, "Пойди туда, не знаю, куда", user2, new DateTime(2022, 10, 25) ),
                           taskFactory.CreateTask("Task02", user2, "Принеси то,не знаю, что", user1,  new DateTime(2022, 09, 26))
                        };

        }
    }

}
