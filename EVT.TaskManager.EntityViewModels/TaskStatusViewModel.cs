﻿

namespace EVT.TaskManager.EntityViewModels
{
    public class TaskStatusViewModel
    {
        public string CurrentStatus { get; }
        public TaskStatusViewModel(Entities.TaskStatus source)
        {
            CurrentStatus = source.CurrentStatus;
        }
    }


}