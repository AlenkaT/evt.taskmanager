﻿

using EVT.TaskManager.Entities;

namespace EVT.TaskManager.EntityViewModels
{
    public class HistoryItemViewModel
    {
        public string Id { get; }
        public string Title { get; }
        public string SubmitterViewName { get; }
        public string SubmitterId { get; }
        
        public string CreationDate { get; }
        public string ParentTaskId { get; }
        public string Text { get; }
        public string UpdateDate { get; }

        public HistoryItemViewModel(HistoryItem source)
        {
            Id = source.Id;
            Title=source.Title;
            SubmitterViewName = source.Submitter.FullName;
            SubmitterId = source.Submitter.Id;
            CreationDate=source.CreationDate.ToString();
            ParentTaskId = source.ParentTask == null ? null : source.ParentTask.Id;
            Text= source.Text;
            UpdateDate = source.UpdateDate.ToString();
    }
    }

    
}