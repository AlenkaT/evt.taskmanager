﻿
using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Entities;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace EVT.TaskManager.EntityViewModels
{
    public class TaskBaseViewModel
    {
        private readonly TaskBase _source;

        private readonly DateTime _creationDate;
        private readonly DateTime? _closeDate;
        public string Id { get; }

        [Required]
        [MinLength(5, ErrorMessage = "Минимально допустимая длина - 5 символов")]
        [Display(Name = "Название задачи")]
        public string Title { get; set; }

        [Display(Name = "Кратное описание")]
        public string? Description { get; set; }

        public string ParentTaskTitle { get; }
        public string ParentTaskId { get; }

        public IEnumerable<KeyValuePair<string, string>> Children { get; }

        public string SubmitterId { get; }

        [Display(Name = "Автор задачи")]
        public string SubmitterViewName { get; }

        public string? HandlerId { get; }
        [Display(Name = "Отвественный")]
        public string? HandlerViewName { get; set; }

        public TaskStatusViewModel Status { get; }

        public ICollection<HistoryItemViewModel> Comments { get; }

        public ICollection<HistoryItemViewModel> History { get; }

        [Display(Name = "Дата создания")]
        public string CreationDate { get; }

        [Display(Name = "Дата обновления")]
        public string UpdateDate { get; }

        [Display(Name = "Сделать до")]
        public string? DeadlineDate { get; set; }

        [Display(Name = "Дата закрытия")]
        public string? CloseDate { get; }

        public string TimeSpan => (_closeDate.HasValue ? _closeDate.Value - _creationDate : DateTime.UtcNow - _creationDate).ToString();


        #region C-tor
        public TaskBaseViewModel(TaskBase source)
        {
            Id = source.Id;
            Title = source.Title;
            Description = source.Description;
            var comments = new List<HistoryItemViewModel>();
            //source.Comments.ToList().ForEach(it => comments.Add(new HistoryItemViewModel(it)));
            Comments = new List<HistoryItemViewModel>(comments);

            var history = new List<HistoryItemViewModel>();
           // source.History.ToList().ForEach(it => history.Add(new HistoryItemViewModel(it)));
            Comments = new List<HistoryItemViewModel>(history);

            var children = new Dictionary<string, string>();
            source.Children.ToList().ForEach(it => children[it.Id] = it.Title);
            Children = new Dictionary<string, string>(children);

            ParentTaskTitle = source.ParentTask.Title;
            ParentTaskId = source.ParentTask == null ? null : source.ParentTask.Id;

            SubmitterId = source.Submitter.Id;
            SubmitterViewName = source.Submitter.FullName;
            if (source.Handler != null)
            {
                HandlerId = source.Handler.Id;
                HandlerViewName = source.Handler.FullName;
            }
            _creationDate = source.CreationDate;
            CreationDate = _creationDate.ToString();
            UpdateDate = source.UpdateDate.ToString();
            DeadlineDate = source.DeadlineDate.HasValue ? source.DeadlineDate.Value.ToString() : String.Empty;
            _closeDate = source.CloseDate;
            CloseDate = _closeDate.HasValue ? _closeDate.Value.ToString() : String.Empty;
            Status = new TaskStatusViewModel(source.Status);
        }
        #endregion


        public override string ToString()
        {
            return this.PrintProperties();
        }

        public virtual TaskBase GetEntity()
        {
            var result = _source;
            return result;
        }
    }
}
