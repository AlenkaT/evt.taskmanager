﻿

using EVT.TaskMamager.MVC.Models;
using EVT.TaskManager.DataAccessLayer;
using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.DataObjects;
using EVT.TaskManager.Entities;

using EVT.TaskManager.Services;
using EVT.TaskManager.Services.Interfaces;
using EVT.TaskManager.Services.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace EVT.TaskMamager.MVC.EntryPoint
{
    internal static class WebApplicationBuilderExtensions
    {
        internal static void DIContainerConfigure(this WebApplicationBuilder builder)
        {

            // Add services to the container.
            var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
            builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(connectionString, b => b.MigrationsAssembly("EVT.TaskManager.MVC")));
            
            builder.Services.AddScoped<IDbInitializer, DbInitializer>();
            builder.Services.AddDatabaseDeveloperPageExceptionFilter();

            builder.Services.AddMvcCore();


            builder.Services.AddIdentity<UserBase, UserRoleBase>(opts =>
                {
                    opts.SignIn.RequireConfirmedAccount = true;
                    opts.SignIn.RequireConfirmedEmail = true;
                    opts.Password.RequiredLength = 8;   // минимальная длина
                    opts.Password.RequireNonAlphanumeric = false;   // требуются ли не алфавитно-цифровые символы
                    opts.Password.RequireLowercase = true; // требуются ли символы в нижнем регистре
                    opts.Password.RequireUppercase = false; // требуются ли символы в верхнем регистре
                    opts.Password.RequireDigit = true; // требуются ли цифры
                }
                ).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

           
            builder.Services.AddScoped<IRepository<TaskBase>, TaskBaseRepository>();
            builder.Services.AddSingleton<TaskFactoryBase>();
            
            builder.Services.AddScoped<IDBManager, DBManager>();
            builder.Services.AddScoped<UserFactoryBase>();
            builder.Services.AddScoped<TaskBaseManager<TaskBase>>();

            builder.Services.AddTransient<IEmailSender, EmailSender>();

            builder.Services.AddRazorPages();
            builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();


            builder.Services.Configure<RouteOptions>(op => { op.LowercaseUrls = true; op.LowercaseQueryStrings = true; op.AppendTrailingSlash = true; });
            builder.Services.Configure<MvcViewOptions>(options =>
                options.HtmlHelperOptions.CheckBoxHiddenInputRenderMode =
                    Microsoft.AspNetCore.Mvc.Rendering.CheckBoxHiddenInputRenderMode.None);

            
            
            



        }
    }
}
