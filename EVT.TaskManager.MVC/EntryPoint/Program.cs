
using EVT.TaskMamager.MVC.EntryPoint;
using EVT.TaskManager.DataAccessLayer;
using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Identity;

var builder = WebApplication.CreateBuilder(args);

builder.DIContainerConfigure();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

 
var scopeFactory = app.Services.GetRequiredService<IServiceScopeFactory>();
using (var scope = scopeFactory.CreateScope())
{
    var dbInitializer = scope.ServiceProvider.GetService<EVT.TaskManager.DataAccessLayer.IDbInitializer>();
    dbInitializer.Initialize();
    dbInitializer.SeedData();
}



app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{userId?}");
/*
app.MapControllerRoute(
    name: "TwoParametrsRoute",
    pattern: "{controller=Home}/{action=Index}/{role}/{id?}");
*/

app.MapRazorPages();

app.Run();
