
using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace EVT.TaskMamager.MVC.Areas.Identity.Pages
{
    [BindProperties]
    public class RegisterModel : PageModel
    {
        private readonly ILogger<RegisterModel> _logger;
        private readonly SignInManager<UserBase> _signInManager;
        private readonly IEmailSender _emailSender;
        private readonly UserFactoryBase _userFactory;

        #region Ctor
        public RegisterModel(ILogger<RegisterModel> logger, SignInManager<UserBase> signInManager, IEmailSender  emailSender, UserFactoryBase userFactory)
        {
            _logger = logger;
            _signInManager = signInManager;
            _emailSender = emailSender;
            _userFactory = userFactory;
        }
        #endregion

        #region Bind Properties

        [MinLength(3, ErrorMessage = "���������� ���������� ����� - 3 �������")]
        [MaxLength(30, ErrorMessage = "����������� ���������� ����� - 30 ��������")]
        [Display(Name = "������������ ��������")]
        public string UserName { get; set; }


        [Required]
        [MaxLength(100, ErrorMessage = "����������� ���������� ����� - 100 ��������")]
        [Display(Name = "������ ��� ������������")]
        public string FullName { get; set; }

        [Required]
        [MinLength(3, ErrorMessage = "���������� ���������� ����� - 3 �������")]
        [MaxLength(50, ErrorMessage = "����������� ���������� ����� - 50 ��������")]
        [RegularExpression(ValidationPatterns.EmailPattern, ErrorMessage = "�������� �� ������������� ������� ����������� �����")]
        public string Email { get; set; }


        [Required]
        [MinLength(8, ErrorMessage = "���������� ���������� ����� ������ - 8 ��������")]
        //��������� ������ �� ��������!!!!!!!!!!!!!!
        //[RegularExpression(ValidationPatterns.PasswordSymLowerPattern, ErrorMessage = "������ ������ ��������� ������ � ������ ��������")]
        [DataType(DataType.Password)]
        [Display(Name = "������")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "������ �� ���������")]
        [DataType(DataType.Password)]
        [Display(Name = "����������� ������")]
        public string ConfirmationPassword { get; set; }

        #endregion

        #region Bind Properies Never

        [BindNever]
        public string Message { get; private set; }
      /*
        [BindNever]
        public string PageHeader { get => "����������� ������ ������������"; }
      */
        #endregion

        #region Requests
        public IActionResult OnGet()
        {
            return Page();
        }



        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> OnPostAsync() // �� �������, ������ ���� �� ��������� ����! (� �������� ��� ��������� private set, ������� �� ��������� ������ �� View
        {
            if (ModelState.IsValid)
            {
                var user =_userFactory.CreateUser(FullName, Email, UserName);
                var result = await _signInManager.UserManager.CreateAsync(user, Password); //�������� ������ ������, ������������� ��� ���������� ������� Identity
                if (result.Succeeded)
                {
                    var code = await _signInManager.UserManager.GenerateEmailConfirmationTokenAsync(user);
                    _logger.LogInformation("ConfirmationToken code: {0}", code);
                    //  await _signInManager.SignInAsync(user, isPersistent: false,  : false);
                    var callbackUrl = Url.PageLink(pageName: "ConfirmEmail", values: new { userId = user.Id, code = "token" });
                    
                    //var callbackUrl = Url.Action("ConfirmEmail", "Account", new { userId = user.Id, code = code }, protocol: Request.Protocol);
                   //�� �������� ��������������
                     await _emailSender.SendEmailAsync(Email, "Email validation",
                               "To finish registration follow <a href=\""
                                                               + callbackUrl.Replace("token", code) + "\">this link</a>");
                    _signInManager.UserManager.AddToRoleAsync(user, UserRoleBase.DefaultUserRole.Name);


                     return RedirectToAction("Index", "Home"); 
                }
                else
                {
                    ModelState.AddModelError("Password", result.ToString());
                    
                }
            }
            else
            {
                Message = "������������ ������";
            }

            return Page();
            
        }
        #endregion


    }
}
