
using EVT.TaskManager.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Areas.Identity.Pages
{

   
    public class LogoutModel : PageModel
    {
        private readonly SignInManager<UserBase> _signInManager;

        public string Message { get; set; }

        public LogoutModel(SignInManager<UserBase> signInManager)
        {
            _signInManager = signInManager;
            Message = "...� ����� ��� ����������...";
        }

        public void OnGet()
        {
        }

        public ActionResult OnPost()
        {
            _signInManager.SignOutAsync();
            return RedirectToAction("Index","Home");
        }
    }
}
