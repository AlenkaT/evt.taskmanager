
using EVT.TaskManager.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Areas.Identity.Pages
{
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly SignInManager<UserBase> _signInManager;

        [BindProperty]
        public string Email { get; set; }
        [BindProperty]
        public string Password { get; set; }
        public bool RememberMe { get; set; }

        
        public string Message { get; set; }

        public LoginModel(SignInManager<UserBase> signInManager)
        {
            _signInManager = signInManager;
            Message = "��������, �� ����������, ��� ����� ����� �����������";
        }

        public void OnGet()
        {

        }

        // [ValidateAntiForgeryToken]
        public async Task<ActionResult> OnPostLoginAsync()
        {
            if (ModelState.IsValid)
            {

                var user = await _signInManager.UserManager.FindByEmailAsync(Email);  //�� ������� ������������
                if (user != null)
                {
                    if (user.EmailConfirmed == true)
                    {
                        var singinResult = await _signInManager.PasswordSignInAsync(user.UserName, Password, RememberMe, lockoutOnFailure: false);
                        if (singinResult.Succeeded)
                        {
                                return RedirectToAction("Index", "Home");
                        }
                        if (singinResult.IsLockedOut)
                        {
                            // return RedirectToAction("Index", "Home");
                            Message = "������������ � ��������� Email ������������!";
                        }
                        if (singinResult.IsNotAllowed)
                        {
                            // return RedirectToAction("Index", "Home");
                            Message = "���� � ������� ��������!";
                        }
                    }
                    else
                    {
                        Message = "����� ������ � ������� ���������� ����������� ���� Email.";
                    }
                }
                else
                {
                    Message = "������������ � ��������� Email �� ���������������";
                }
            }
            else
            {
                Message = "������������ ������";
            }
            return Page();
        }
    }
}
