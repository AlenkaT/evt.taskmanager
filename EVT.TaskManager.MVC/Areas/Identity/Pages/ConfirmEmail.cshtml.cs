
using EVT.TaskManager.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Areas.Identity.Pages
{
    public class ConfirmEmailModel : PageModel
    {
        private readonly ILogger<RegisterModel> _logger;
        private readonly SignInManager<UserBase> _signInManager;

        public ConfirmEmailModel(ILogger<RegisterModel> logger, SignInManager<UserBase> signInManager)
        {
            _logger = logger;
            _signInManager = signInManager;
        }

        [AllowAnonymous]
        public async Task<ActionResult> OnGetAsync(string userId, string code) //��� ��������� � ������� �������� , � ���� ��������� � ������
        {
            if (userId == null || code == null)
            {
                return RedirectToAction("Error", "Home");
            }
            var user = await _signInManager.UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                //������������ �� ������
                _logger.LogError("������������ � id: {0} �� ������", userId);
                return RedirectToAction("Error", "Home");
            }
            var result = await _signInManager.UserManager.ConfirmEmailAsync(user, code.Replace(" ","+"));
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                _logger.LogError("Invalid code: {0}", code);
                return RedirectToAction("Error", "Home");
            }

        }

    }
}
