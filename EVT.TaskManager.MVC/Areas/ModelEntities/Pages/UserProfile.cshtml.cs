using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.Services;
using EVT.TaskManager.Specifications;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Areas.ModelEntities.Pages
{
    public class UserProfileModel : PageModel
    {
        private readonly ILogger<UserProfileModel> _logger;
        private readonly IDBManager _dbManager;
        private readonly UserManager<UserBase> _userManager;
        
       // private readonly RoleManager<UserRoleBase> _roleManager;

        #region Ctor
        public UserProfileModel(ILogger<UserProfileModel> logger, IDBManager dbManager, UserManager<UserBase> userManager)//, RoleManager<UserRoleBase> roleManager)
        {
            _logger = logger;
            _dbManager = dbManager;
            _userManager = userManager;
           // _roleManager = roleManager;
        }
        #endregion

        #region Public Properties
        public UserBase CurrentUser { get; private set; }

        public TaskBase[] Tasks { get; private set; }

        public string Roles { get; private set; }
        #endregion


        public async Task<IActionResult> OnGetAsync(string? name)
        {

            CurrentUser = await _userManager.FindByNameAsync(name);
            if (CurrentUser  == null) return RedirectToPage("NotFound_404", new { area = "Messages" });
            var roles = await _userManager.GetRolesAsync(CurrentUser);
            
            if (roles != null)
            {
                Roles=String.Join(";", roles);
            }

            Tasks = _dbManager.FindEntities<TaskBase>(new TasksForUserIdSpec(CurrentUser.Id)).ToArray(); //������� �� async
            
            return Page();
        }

      
    }
}
