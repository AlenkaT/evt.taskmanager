using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.EntityViewModels;

using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace EVT.TaskMamager.MVC.Areas.ModelEntities.Pages
{
    public class TaskEditModel : PageModel
    {
        #region Private Fields
        private readonly TaskBaseManager<TaskBase> _taskManager; //��� ����������
        private readonly IDBManager _dbManager; //��� ������ ������
        private readonly UserManager<UserBase> _userManager;
        #endregion

        public TaskBaseViewModel CurrentTask { get; private set; }


        public string Message { get; private set; }


        public SelectList UserList { get; private set; }



        public async Task<IActionResult> OnGetAsync(string id)
        {
            CurrentTask = new (await _dbManager.GetEntityByIdAsync<TaskBase>(id));

            if (CurrentTask == null)
            {
                return RedirectToPage("NotFound_404", new { area = "Messages" });
            }
            UserList = new SelectList(_userManager.Users, nameof(UserBase.UserName), nameof(UserBase.FullName));

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            CurrentTask = new TaskBaseViewModel( await _dbManager.GetEntityByIdAsync<TaskBase>(id));
            if (ModelState.IsValid)
            {
                /*
                if (CurrentTask != null && await _taskManager.TryUpdateAsync(HttpContext.User.Identity.Name, CurrentTask))
                {
                    Message = "������ ������� ���������";
                }
                else
                {
                    Message = "���� ��������";
                }
                */
            }
            else
            {
                Message = "������������ ������";
            }

            return Page();
        }

        

        #region Ctor
        public TaskEditModel(IDBManager dbManager, TaskBaseManager<TaskBase> taskManager, UserManager<UserBase> userManager)
        {
            _taskManager = taskManager;
            _dbManager = dbManager;
            _userManager = userManager;
        }
        #endregion
    }
}
