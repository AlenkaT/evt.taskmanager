using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Areas.ModelEntities.Pages
{
    public class UserListModel : PageModel
    {

        private readonly ILogger<UserListModel> _logger;
        private readonly IDBManager _dbManager;
        private readonly UserManager<UserBase> _manager;


        #region Binding
        [BindProperty]
        public VisualItem[] VisualItems { get; set; }
        
        #endregion

        #region C-tor
        public UserListModel(ILogger<UserListModel> logger, IDBManager dbManager, UserManager<UserBase> manager)
        {
            _logger = logger;
            _dbManager= dbManager;
            _manager = manager;
        }
        #endregion


        #region Requests

        public void OnGet()
        {
            var list = new List<VisualItem>();
            foreach (var item in _manager.Users)
            {
                list.Add(new VisualItem() { ID = item.Id, IsSelected = false, User=item });
            }

            VisualItems = list.ToArray();

        }


        public IActionResult OnPostDeactivateUsers()
        {
            foreach (var item in VisualItems)
            {
                //if (item.IsSelected) _facade.Deactivate(item.User); 
            }
            return RedirectToPage();
        }

        #endregion


        public class VisualItem
        {

            public string ID { get; set; }

            public bool IsSelected { get; set; }

            public UserBase User { get; set; }

        }



    }
}
