using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace EVT.TaskMamager.MVC.Areas.ModelEntities.Pages
{
    public class UserProfileEditModel : PageModel
    {
        private readonly ILogger<UserProfileModel> _logger;
        private readonly UserManager<UserBase> _userManager;
        private readonly RoleManager<UserRoleBase> _roleManager;

        public SelectList UserRoleList { get; }
        public UserProfileEditModel(ILogger<UserProfileModel> logger, UserManager<UserBase> userMsnger, RoleManager<UserRoleBase> roleManager)
        {
            _logger = logger;
            _userManager = userMsnger;
            _roleManager = roleManager;

            UserRoleList = new SelectList(_roleManager.Roles, nameof(UserRoleBase.Id), nameof(UserRoleBase.Name));
        }


        [BindProperty]
        public UserBase CurrentUser { get; set; }



        private string _pageHeader = "";
        [BindProperty]
        public string PageHeader => _pageHeader;

        [BindProperty]
        public string Message { get; set; }



        #region Requests
        public async Task<IActionResult> OnGetAsync(string name)
        {
            var user = await _userManager.FindByNameAsync(name);

            if (user == null) return RedirectToPage("NotFound_404", new { area = "Messages" });

            CurrentUser = user;

            _pageHeader = "��������� ������ ������������";
            return Page();
        }



        public async Task<IActionResult> OnPostAsync() // �� �������, ������ ���� �� ��������� ����! (� �������� ��� ��������� private set, ������� �� ��������� ������ �� View
        {

            var user = await _userManager.FindByNameAsync(CurrentUser.UserName);

            if (user!=null)
            {
                // ���-�� ��������, ��� ������ ������������ ���� ���������
            }
            else
            {
                // �������� �� ������ ����������
            }

            return Page();
        }
        /*
                public void OnPostUpdateNotificationPreferencs(Guid id)
                {
                    if (Notify) Message = "Thank you";
                    else
                        Message = "You have turned of email notifications";
                    CurrentUser = _userDb.GetItem(id);
                }

                */
        /*
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (_userDb.TryUpdate(CurrentUser, out updatedUser))
            {
                CurrentUser = updatedUser;
                // ���-�� ��������, ��� ������ ������������ ���� ���������
            }
            _context.Customers.Add(Customer);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
        */
        #endregion


    }
}
