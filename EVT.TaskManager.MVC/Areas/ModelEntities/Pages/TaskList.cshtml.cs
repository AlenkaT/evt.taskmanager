using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Areas.ModelEntities.Pages
{
    public class TaskListModel: PageModel
    {
        private readonly ILogger<TaskListModel> _logger;
        private readonly IDBManager _dbManager;
        public TaskListModel(ILogger<TaskListModel> logger, IDBManager dbManager)
        {
            _logger = logger;
            _dbManager = dbManager;
        }


        public IEnumerable<TaskBase> Tasks;

        public async Task<IActionResult> OnGetAsync()
        {
            Tasks = await _dbManager.GetEntitiesAsync<TaskBase>();
            return Page();
        }
    }
}
