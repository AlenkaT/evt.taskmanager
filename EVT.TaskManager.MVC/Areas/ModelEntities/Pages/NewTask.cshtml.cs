using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace EVT.TaskMamager.MVC.Areas.ModelEntities
{
    [BindProperties]
    public class NewTaskModel : PageModel
    {
        private readonly ILogger<NewTaskModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IDBManager _dbManager;
        private readonly UserManager<UserBase> _userManager;
        private readonly TaskFactoryBase _taskFactory;

        #region Ctor
        public NewTaskModel(ILogger<NewTaskModel> logger, IEmailSender emailSender, UserManager<UserBase> userManager, IDBManager dbManager, TaskFactoryBase taskFactory)
        {
            _logger = logger;
            _dbManager = dbManager;
            _emailSender = emailSender;
            _userManager = userManager;
            _taskFactory = taskFactory;
        }
        #endregion


        #region Bind Properties

        [Required]
        [MinLength(5, ErrorMessage = "���������� ���������� ����� - 5 ��������")]
        [Display(Name = "�������� ������")]
        public string TaskTitle { get; set; }

        [Display(Name = "������� ��������")]
        public string Description { get; set; }

        [DataType(DataType.Date)]
       // [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DeadlineDate { get; set; }


        [Display(Name = "��������������")]
        public string? HandlerUserName { get; set; }

        #endregion

        [BindNever]
        public SelectList HandlersList { get => new SelectList(_userManager.Users, nameof(UserBase.Id), nameof(UserBase.FullName)); }

       


        public async Task<IActionResult> OnGetAsync(string? name)
        {
            if (name != null)
            {
                var handler = await _userManager.FindByNameAsync(name);
                if (handler != null)
                {
                    HandlerUserName = handler.UserName;
                }
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var submitter = await _userManager.FindByNameAsync(HttpContext.User.Identity.Name);
                var handler = await _userManager.FindByNameAsync(HandlerUserName);
                var newTask = _taskFactory.CreateTask(TaskTitle, submitter, Description, handler, DeadlineDate);
                await _dbManager.AddEntityAsync<TaskBase>(newTask);
                if (handler == null)
                {
                    return RedirectToPage("TaskList");
                }
                else
                {
                    return RedirectToPage("UserProfile" , new { name = HandlerUserName });
                }
            }
            else return Page();
        }
    }



}
