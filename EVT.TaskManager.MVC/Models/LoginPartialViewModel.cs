using EVT.TaskManager.ModelEntities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace EVT.TaskMamager.MVC.Models
{
    public class LoginPartialViewModel : PageModel
    {
        private readonly SignInManager<UserBase> _signInManager;
        private readonly UserManager<UserBase> _userManager;

        public bool IsUserSignedIn { get => _signInManager.IsSignedIn(User); }
        public LoginPartialViewModel(SignInManager<UserBase> signInManager,  UserManager<UserBase> userManager)
        {
            _signInManager=signInManager;
            _userManager=userManager;
        }

    }
}