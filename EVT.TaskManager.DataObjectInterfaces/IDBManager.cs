﻿using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.Services;
using System.Linq;

namespace EVT.TaskManager.DataObjectInterfaces
{
    public interface IDBManager
    {
        Task<TEntity> GetEntityByIdAsync<TEntity>(string id) where TEntity : class, IIdentity;
        Task AddEntityAsync<TEntity>(TEntity item) where TEntity : class, IIdentity;
        Task UpdateEntityAsync<TEntity>(TEntity item) where TEntity : class, IIdentity;
        Task DeleteEntityByIdAsync<TEntity>(string id) where TEntity : class, IIdentity;
        Task<IQueryable<TEntity>> GetEntitiesAsync<TEntity>(int startIndex = 0, int? count = null) where TEntity : class, IIdentity;
        IEnumerable<TEntity> FindEntities<TEntity>(Specifications.ISpecification<TEntity> spec) where TEntity : class, IIdentity;

    }
}