﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications
{
    internal class OrSpecification<TEntity> : SpecificationBase<TEntity> where TEntity : class
    {
        //private readonly ISpecification<TEntity> _leftCondition;
        private readonly Expression<Func<TEntity, bool>>  _rightCondition;
        public OrSpecification(ISpecification<TEntity> leftCondition, ISpecification<TEntity> rightCondition):base(leftCondition.Condition)
        {
            //_leftCondition = leftCondition;
            //_rightCondition = rightCondition;
        }


        protected override bool DoIsSatisfiedBy(TEntity candidate)
        {
            Func<TEntity, bool> predicate = _rightCondition.Compile();
            return base.IsSatisfiedBy(candidate) || predicate(candidate);
           
        }
        
    }
}
