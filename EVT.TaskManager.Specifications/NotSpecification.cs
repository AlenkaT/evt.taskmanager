﻿using System.Linq.Expressions;

namespace EVT.TaskManager.Specifications
{
    internal class NotSpecification<TEntity> : SpecificationBase<TEntity> where TEntity : class
    {
        public NotSpecification(ISpecification<TEntity> condition) : base(condition.Condition)
        {
        }

        protected override bool DoIsSatisfiedBy(TEntity candidate)
        {

            return !base.IsSatisfiedBy(candidate);

        }
    }
}