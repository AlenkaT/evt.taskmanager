﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications
{
    public abstract class SpecificationBase<TEntity> : ISpecification<TEntity> where TEntity : class
    {
        private readonly Expression<Func<TEntity, bool>> _condition;
        Expression<Func<TEntity, bool>> ISpecification<TEntity>.Condition => _condition;

        public SpecificationBase(Expression<Func<TEntity, bool>> condition)
        {
            _condition = condition;
        }
        
        protected virtual bool DoIsSatisfiedBy(TEntity candidate)
        {
            Func<TEntity, bool> predicate =_condition.Compile(); 
            return predicate(candidate);
        }

        public bool IsSatisfiedBy(TEntity candidate)
        {
            return DoIsSatisfiedBy(candidate);
        }

        public ISpecification<TEntity> And(ISpecification<TEntity> other)
        {
            
            return new AndSpecification<TEntity>(this, other);
        }
        /*
        public ISpecification<TEntity> AndNot(ISpecification<TEntity> other)
        {
            return new AndNotSpecification<TEntity>(this, other);
        }
        */
        public ISpecification<TEntity> Or(ISpecification<TEntity> other)
        {
            return new OrSpecification<TEntity>(this, other);
        }

        /*public ISpecification<TEntity> OrNot(ISpecification<TEntity> other)
        {
            return new OrNotSpecification<TEntity>(this, other);
        }
        */
        public ISpecification<TEntity> Not()
        {
            return new NotSpecification<TEntity>(this);
        }

        
    }
}
