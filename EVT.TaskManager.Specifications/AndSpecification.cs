﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications
{
    internal class AndSpecification<TEntity> : SpecificationBase<TEntity> where TEntity : class
    {
       
        private readonly Expression<Func<TEntity, bool>> _rightCondition;
        public AndSpecification(ISpecification<TEntity> leftCondition, ISpecification<TEntity> rightCondition):base (leftCondition.Condition)
        {
            _rightCondition = rightCondition.Condition;
        }

        protected override bool DoIsSatisfiedBy(TEntity candidate)
        {
            Func<TEntity, bool> predicate = _rightCondition.Compile();
            return base.IsSatisfiedBy(candidate) && predicate(candidate);
        }
        
    }
}
