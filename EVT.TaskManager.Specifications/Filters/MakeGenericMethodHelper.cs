﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications.Filters
{
    

    internal class GenericMethodMaker
    {
        internal static Expression GetExpressionForConditionType(TypeFilterConditionForValueTypePropertyEnum typeCondition, MemberExpression propertyExpression, Expression valueExpression)
        {
            switch (typeCondition)
            {
                case TypeFilterConditionForValueTypePropertyEnum.Equal:
                    { 
                        return Expression.Equal(propertyExpression, valueExpression); 
                    }
                case TypeFilterConditionForValueTypePropertyEnum.NotEqual:
                    { 
                        return Expression.NotEqual(propertyExpression, valueExpression); 
                    }
                case TypeFilterConditionForValueTypePropertyEnum.Less:
                    {
                        return Expression.LessThan(propertyExpression, valueExpression);
                    }
                case TypeFilterConditionForValueTypePropertyEnum.LessOrEqual:
                    {
                        return Expression.LessThanOrEqual(propertyExpression, valueExpression);
                    }
                case TypeFilterConditionForValueTypePropertyEnum.Greater:
                    {
                        return Expression.GreaterThan(propertyExpression, valueExpression);
                    }
                case TypeFilterConditionForValueTypePropertyEnum.GreaterOrEqual:
                    {
                        return Expression.GreaterThanOrEqual(propertyExpression, valueExpression);
                    }
            }
            return null;
        }

        internal static LambdaExpression GetLambdaExpressionForPropertyValue(Type recordDBType, String propertyName, object propertyValue, TypeFilterConditionForValueTypePropertyEnum typeCondition)
        {
            ParameterExpression parameterExpression = Expression.Parameter(recordDBType, "x");
            Expression propertyExpression = GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));
            
            if (propertyExpression==null)
            {
                throw new ArgumentException(message: "Значение типа фильтра не установлено", paramName: nameof(typeCondition));
            }
            var lambda = Expression.Lambda(propertyExpression, parameterExpression);
            return lambda;
        }

        

        internal static MethodInfo GetMethodInfoForQueryableWhere(Type recordDBType) //recordDBType - тип записи базы данных
        {
            var methodInfo = typeof(Queryable).GetMethods().FirstOrDefault(x => x.Name == "Where");
            //metodInfo = metodInfo.MakeGenericMethod(recordDBType, lambda.Body.Type);
            methodInfo = methodInfo.MakeGenericMethod(recordDBType);

            return methodInfo;
        }

        
        internal static object InvokeGenericQueryableMethodWithSelectorExpression(Type recordDBType, String propertyName, object propertyValue, object source, TypeFilterConditionForValueTypePropertyEnum typeCondition)
        {
            
            var methodInfo = GenericMethodMaker.GetMethodInfoForQueryableWhere(recordDBType);
            var lambda = GenericMethodMaker.GetLambdaExpressionForPropertyValue(recordDBType, propertyName, propertyValue, typeCondition);
            var result = methodInfo.Invoke(null, new[] { source, lambda });
            return result;
        }

        

    }
    //оТ UI приходят данные - наименование поля фильтразии; тип условия фильтрации; значение, относительного которого нужно выполнить фильтрацию
    /*
     * По названию поля находим ссылку на бд, где лежит сущность
     * 
     * 
     *после фильтрации и получения списка объектов
     * Нужно как-то сформировать список полей для вывода, которые будут вытягиваться из базы и которые запрашивает вью
     * */
}
