﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications.Filters
{
    public enum TypeFilterConditionForValueTypePropertyEnum
    {

        Equal,
        NotEqual,
        Less,
        LessOrEqual,
        Greater,
        GreaterOrEqual

    }
}
