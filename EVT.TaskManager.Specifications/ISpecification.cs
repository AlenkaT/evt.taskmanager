﻿using System.Linq.Expressions;

namespace EVT.TaskManager.Specifications
{
    public interface ISpecification<TEntity> where TEntity : class
    {
        internal Expression<Func<TEntity, bool>> Condition { get; }
        bool IsSatisfiedBy(TEntity candidate);
        ISpecification<TEntity> And(ISpecification<TEntity> other);
      //  ISpecification<TEntity> AndNot(ISpecification<TEntity> other);
        ISpecification<TEntity> Or(ISpecification<TEntity> other);
       // ISpecification<TEntity> OrNot(ISpecification<TEntity> other);
        ISpecification<TEntity> Not();
        
    }
}