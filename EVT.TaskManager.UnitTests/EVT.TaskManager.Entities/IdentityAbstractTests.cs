﻿using EVT.TaskManager.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.UnitTests.Entities
{

    /*
    internal class IdentityAbstractHelper: IdentityAbstract
    {
        class IdentityTest: IdentityAbstract
        {

        }
        static readonly IdentityAbstract Instance = new IdentityTest();

        private IdentityAbstract _privateField = null;
        protected IdentityAbstract _protectedField = null;
        internal IdentityAbstract _internalField = null;
        public IdentityAbstract _publicField = null;

        public IdentityAbstract PrivateField => _privateField;
        public IdentityAbstract ProtectedField => _protectedField;
        public IdentityAbstract InternalField => _internalField;
        public IdentityAbstract PublicField => _publicField;


        private string _Message = "This is a helper class";
        public string Message => "_This is a helper class_";

        public IdentityAbstractHelper()
        {
            _privateField = Instance;
            _protectedField = Instance;
            _internalField = Instance;
            _publicField = Instance;
           
        }
    }

    [TestFixture]
    public class IdentityAbstractTests
    {
        
        [Test]
        public void Clone_HashCodesNotEqualExpacted()
        {
            //Arrange
            var testObj = new IdentityAbstractHelper();

            //Act
            var cloneObj = testObj.Clone() as IdentityAbstractHelper;

            //Assert
            Assert.IsFalse(Object.ReferenceEquals(testObj, cloneObj));
            Assert.IsFalse(Object.ReferenceEquals(testObj.PrivateField, cloneObj.PrivateField));
            Assert.IsFalse(Object.ReferenceEquals(testObj.ProtectedField, cloneObj.ProtectedField));
            Assert.IsFalse(Object.ReferenceEquals(testObj.InternalField, cloneObj.InternalField));
            Assert.IsFalse(Object.ReferenceEquals(testObj.PublicField, cloneObj.PublicField));
            
        }
        
    }
    */
}





