﻿using EVT.TaskManager.Entities;
using EVT.TaskManager.MockRepositories;

using EVT.TaskManager.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;

using NUnit.Framework;


namespace EVT.TaskManager.Services.Tests
{
    class UserStoreTestHelper : IUserStore<UserBase>
    {
        #region interface methods NotImplementedException
        public Task<IdentityResult> CreateAsync(UserBase user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> DeleteAsync(UserBase user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public Task<UserBase> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<UserBase> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetNormalizedUserNameAsync(UserBase user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetUserIdAsync(UserBase user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetUserNameAsync(UserBase user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetNormalizedUserNameAsync(UserBase user, string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task SetUserNameAsync(UserBase user, string userName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<IdentityResult> UpdateAsync(UserBase user, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
    class UserManagerTestHelper : UserManager<UserBase>
    {
        private readonly List<UserBase> _users = new List<UserBase>(MockInitData.Instance.Users.Union(MockInitData.Instance.Admins));

        public override Task<UserBase> FindByNameAsync(string userName)
        {
            return Task<UserBase>.Factory.StartNew(() => _users.FirstOrDefault(x => x.UserName == userName));
        }

        public UserManagerTestHelper()
            : base(new UserStoreTestHelper(), null, null, null, null, null, null, null, null)
        {
        }
    }
    //Protected virtual methods testing
   
    class TaskBaseManagerTestHelper : TaskBaseManager<TaskBase>
    {
        public TaskBaseManagerTestHelper(ILogger<TaskBaseManagerTestHelper> logger, IRepository<TaskBase> taskRepository, UserManager<UserBase> userManager) : base(logger, taskRepository, userManager)
        {
        }

        internal void InvokeDoUpdateCloseDate(TaskBase source, DateTime? closeDate)
        {
            DoUpdateCloseDate(source, closeDate);
        }
        internal void InvokeDoUpdateDeadlineDate(TaskBase source, DateTime? closeDate)
        {
            DoUpdateDeadlineDate(source, closeDate);
        }

        internal void InvokeDoUpdateDescription(TaskBase source, string? description)
        {
            DoUpdateDescription(source, description);
        }

        internal async void InvokeDoUpdateHandlerAsync(TaskBase source, string handlerUserName)
        {
            await DoUpdateHandlerAsync(source, handlerUserName);
        }

        internal async void InvokeDoUpdateParentTaskAsync(TaskBase source, string parentTaskId)
        {
            await DoUpdateParentTaskAsync(source, parentTaskId);
        }
        internal void InvokeDoUpdateStatus(TaskBase source, string status)
        {
            DoUpdateStatus(source, status);
        }
        internal void InvokeDoUpdateTitle(TaskBase source, string title)
        {
            DoUpdateTitle(source, title);
        }

        internal void InvokeDoLogMethodResult(string methodName, params object[]? result)
        {
            DoLogMethodResult(methodName, result);
        }
        internal void InvokeDoLogMethodResultWarn(string methodName, params object[]? result)
        {
            this.DoLogMethodResultWarn(methodName, result);
        }

        internal  TaskBase InvokeDoGetTaskFromRepositoryForUser(string userName, string taskId)
        {
           return DoGetTaskFromRepositoryForUserAsync(userName, taskId).Result;
        }
    }

    [TestFixture]
    public class TaskBaseManagerUnitTests
    {
        [Test]
        public void TryUpdateAsync_WithoutLogging_WhenSourceIsNull_FalseExpacted()
        {
            //Arrange
            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManager<TaskBase>> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManager<TaskBase>(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;

            //Act
            var result = taskManager.TryUpdateAsync(userNameFromContext, null).Result;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void TryUpdateAsync_WithoutLogging_WhenUserNameFromContextIsNull_FalseExpacted()
        {
            //Arrange
            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManager<TaskBase>> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManager<TaskBase>(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            //Act
            var result = taskManager.TryUpdateAsync(null, source).Result;

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void TryUpdateAsync_WithoutLogging_WhenStoreContainsTaskAndUser_TrueExpacted()
        {
            //Arrange
            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManager<TaskBase>> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManager<TaskBase>(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            var source = new TaskBase("TitleTest", MockInitData.Instance.Admins.First(),"DescriptionTest", MockInitData.Instance.Users.First(), DateTime.Now);
            repository.AddAsync(source);
            //Act
            bool result = taskManager.TryUpdateAsync(userNameFromContext, source).Result;

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void DoUpdateTitle_WithoutLogging_TaskTitleUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            var task1 = repository.AddAsync(source);
            //Act
            var newTitleValue = "NewTitleTest";
            source.Title = newTitleValue;
            taskManager.InvokeDoUpdateTitle(source, newTitleValue);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(newTitleValue, actual.Title);
        }

        [Test]
        public void DoUpdateDescription_WithoutLogging_TaskDescriptionUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            var task1 = repository.AddAsync(source);
            //Act
            var newDescriptionValue = "NewDescriptionTest";
            source.Description = newDescriptionValue;
            taskManager.InvokeDoUpdateDescription(source, newDescriptionValue);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(newDescriptionValue, actual.Description);
        }

        [Test]
        public void DoUpdateCloseDate_WithoutLogging_TaskCloseDateUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            var task1 = repository.AddAsync(source);
            //Act
            var newCloseDateValue = DateTime.UtcNow;
            source.CloseDate = newCloseDateValue;
            taskManager.InvokeDoUpdateCloseDate(source, newCloseDateValue);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(newCloseDateValue, actual.CloseDate);
        }

        [Test]
        public void DoUpdateDeadlineDate_WithoutLogging_TaskDeadlineDateUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            var task1 = repository.AddAsync(source);
            //Act
            var newDeadlineDateValue = DateTime.UtcNow;
            source.DeadlineDate = newDeadlineDateValue;
            taskManager.InvokeDoUpdateDeadlineDate(source, newDeadlineDateValue);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(newDeadlineDateValue, actual.DeadlineDate);
        }

        [Test]
        public void DoUpdateParentTask_WithoutLogging_TaskParentTaskUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;

            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            var task1 = repository.AddAsync(source);
            //Act
            var newParentTask = new TaskBase("ParentTask", MockInitData.Instance.Users.First(), null, null, null);
            source.ParentTask = newParentTask;
            taskManager.InvokeDoUpdateParentTaskAsync(source, newParentTask.Id);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(newParentTask, actual.ParentTask);
        }

        [Test]
        public void DoUpdateHandler_WithoutLogging_TaskHandlerUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;

            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            var task1 = repository.AddAsync(source);
            //Act
            var newHandler = MockInitData.Instance.Admins.First();
            source.Handler = newHandler;
            taskManager.InvokeDoUpdateHandlerAsync(source, newHandler.UserName);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(newHandler, actual.Handler);
        }
        
        [Test]
        public void DoUpdateStatus_WithoutLogging_TaskStatusUpdatingExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;

            var startStatus = "new";
            var endStatus = "done";
            var status= new Entities.TaskStatus(new string[] { startStatus, endStatus });

            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null, status);
            var task1 = repository.AddAsync(source);
            //Act
            
            taskManager.InvokeDoUpdateStatus(source, endStatus);

            //Assert
            var actual = repository.GetItemByIdAsync(source.Id).Result;
            Assert.AreEqual(endStatus, actual.Status.CurrentStatus);
        }
       
        [Test]
        public void DoGetTaskFromRepositoryForUser_WithoutLogging_WhenUserNameIsNull_NullExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);               
            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            
            //Act

            var task=taskManager.InvokeDoGetTaskFromRepositoryForUser(null, source.Id);

            //Assert

            Assert.IsNull(task);
        }

        [Test]
        public void DoGetTaskFromRepositoryForUser_WithoutLogging_WhenTaskIdIsNull_NullExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;
            
            //Act

            var task = taskManager.InvokeDoGetTaskFromRepositoryForUser(userNameFromContext, null);

            //Assert

            Assert.IsNull(task);
        }

        [Test]
        public void DoGetTaskFromRepositoryForUser_WithoutLogging_WhenTaskIdDoNotExist_NullExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;

            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            
            //Act

            var task = taskManager.InvokeDoGetTaskFromRepositoryForUser(userNameFromContext, source.Id);

            //Assert

            Assert.IsNull(task);
        }

        [Test]
        public void DoGetTaskFromRepositoryForUser_WithoutLogging_WhenUserdDoNotExist_NullExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = "UserTest"+Guid.NewGuid().ToString();

            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            repository.AddAsync(source);    
            //Act

            var task = taskManager.InvokeDoGetTaskFromRepositoryForUser(userNameFromContext, source.Id);

            //Assert

            Assert.IsNull(task);
        }

        [Test]
        public void DoGetTaskFromRepositoryForUser_WithoutLogging_WhenUserAndTaskExist_TaskBaseExpacted()
        {
            //Arrange

            var userManager = new UserManagerTestHelper();
            ILogger<TaskBaseManagerTestHelper> loggerUserManager = null;
            ILogger<MockTaskRepository> loggerTaskRepository = null;
            var repository = new MockTaskRepository(loggerTaskRepository);
            var taskManager = new TaskBaseManagerTestHelper(loggerUserManager, repository, userManager);
            var userNameFromContext = MockInitData.Instance.Admins.First().UserName;

            var source = new TaskBase("Test", MockInitData.Instance.Users.First(), null, null, null);
            repository.AddAsync(source);
            //Act

            var task = taskManager.InvokeDoGetTaskFromRepositoryForUser(userNameFromContext, source.Id);

            //Assert

            Assert.AreEqual(source, task);
        }

    }
}
