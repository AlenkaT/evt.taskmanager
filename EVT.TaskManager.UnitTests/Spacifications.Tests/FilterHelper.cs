﻿using EVT.TaskManager.Entities;
using EVT.TaskManager.Specifications.Filters;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications.Filters.Tests
{
    enum TestEnum
    {
        f1,
        f2,
        f3
    }
    class GetLambdaExpressionTestHelper
    {
        public Boolean BooleanProperty { get; set; }
        public int IntProperty { get; set; }
        public double DoubleProperty { get; set; }
        public Guid GuidProperty { get; set; }
        public TestEnum TestEnumProperty { get; set; }
        public string StringProperty { get; set; }
        public DateTime DateTimeProperty { get; set; }

    }

    [TestFixture]
    public class GenericMethodMaker_UnitTests
    {
        [Test]
        public void GetExpressionForConditionType_WhenTypeConditionIsEqual_ResultContainsEqualOperationExpacted()
        {
            //Arrange
            var typeCondition =TypeFilterConditionForValueTypePropertyEnum.Equal;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(GetLambdaExpressionTestHelper), "x");
            var propertyName = nameof(GetLambdaExpressionTestHelper.IntProperty);
            var propertyValue = default(int);
            
            //Act

            var result = GenericMethodMaker.GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));

            //Assert
            Assert.That(result.ToString(), Contains.Substring(propertyName + " == "+ propertyValue));
        }

        [Test]
        public void GetExpressionForConditionType_WhenTypeConditionIsNotEqual_ResultContainsNotEqualOperationExpacted()
        {
            //Arrange
            var typeCondition = TypeFilterConditionForValueTypePropertyEnum.NotEqual;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(GetLambdaExpressionTestHelper), "x");
            var propertyName = nameof(GetLambdaExpressionTestHelper.IntProperty);
            var propertyValue = default(int);

            //Act

            var result = GenericMethodMaker.GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));

            //Assert
            Assert.That(result.ToString(), Contains.Substring(propertyName + " != "+ propertyValue));
        }

        [Test]
        public void GetExpressionForConditionType_WhenTypeConditionIsLess_ResultContainsLessOperationExpacted()
        {
            //Arrange
            var typeCondition = TypeFilterConditionForValueTypePropertyEnum.Less;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(GetLambdaExpressionTestHelper), "x");
            var propertyName = nameof(GetLambdaExpressionTestHelper.IntProperty);
            var propertyValue = default(int);

            //Act

            var result = GenericMethodMaker.GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));

            //Assert
            Assert.That(result.ToString(), Contains.Substring(propertyName + " < "+ propertyValue));
        }

        [Test]
        public void GetExpressionForConditionType_WhenTypeConditionIsLessOrEqual_ResultContainsLessOrEqualOperationExpacted()
        {
            //Arrange
            var typeCondition = TypeFilterConditionForValueTypePropertyEnum.LessOrEqual;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(GetLambdaExpressionTestHelper), "x");
            var propertyName = nameof(GetLambdaExpressionTestHelper.IntProperty);
            var propertyValue = default(int);

            //Act

            var result = GenericMethodMaker.GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));

            //Assert
            Assert.That(result.ToString(), Contains.Substring(propertyName + " <= "+ propertyValue));
        }


        [Test]
        public void GetExpressionForConditionType_WhenTypeConditionIsGreater_ResultContainsGreaterOperationExpacted()
        {
            //Arrange
            var typeCondition = TypeFilterConditionForValueTypePropertyEnum.Greater;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(GetLambdaExpressionTestHelper), "x");
            var propertyName = nameof(GetLambdaExpressionTestHelper.IntProperty);
            var propertyValue = default(int);

            //Act

            var result = GenericMethodMaker.GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));

            //Assert
            Assert.That(result.ToString(), Contains.Substring(propertyName + " > "+ propertyValue));
        }

        [Test]
        public void GetExpressionForConditionType_WhenTypeConditionIsGreaterOrEqual_ResultContainsGreaterOrEqualOperationExpacted()
        {
            //Arrange
            var typeCondition = TypeFilterConditionForValueTypePropertyEnum.GreaterOrEqual;
            ParameterExpression parameterExpression = Expression.Parameter(typeof(GetLambdaExpressionTestHelper), "x");
            var propertyName = nameof(GetLambdaExpressionTestHelper.IntProperty);
            var propertyValue = default(int);

            //Act

            var result = GenericMethodMaker.GetExpressionForConditionType(typeCondition, Expression.Property(parameterExpression, propertyName), Expression.Constant(propertyValue));

            //Assert
            Assert.That(result.ToString(), Contains.Substring(propertyName+" >= "+ propertyValue));
        }
       
        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsBoolean_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);
            //Act

            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.BooleanProperty), default(bool), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsInt_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);
            //Act

            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.IntProperty), default(int), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsDouble_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);
            //Act

            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.DoubleProperty), default(double), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsDateTime_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.DateTimeProperty), default(DateTime), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsString_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.StringProperty), default(string), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsGuid_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.GuidProperty), default(Guid), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsEnum_TrueExpacted()
        {
            //Arrange
            var testObj = new GetLambdaExpressionTestHelper();
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act

            var result = GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.TestEnumProperty), default(TestEnum), typeCondition);
            var invokeResult = result.Compile().DynamicInvoke(testObj);

            //Assert
            Assert.IsNotNull(invokeResult);
            Assert.IsTrue(invokeResult as bool?);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsIntButPropertyValueIsDouble_InvalidOperationExceptionExpacted()
        {
            //Arrange
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            //Тип свойства и тип переданного значения не совпадают
            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.IntProperty), default(double), typeCondition));
            //Assert
            Assert.Catch<InvalidOperationException>(testDelegate);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsEnumButPropertyValueIsInt_InvalidOperationExceptionExpacted()
        {
            //Arrange
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            //Тип свойства и тип переданного значения не совпадают
            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.TestEnumProperty), default(int), typeCondition));
            //Assert
            Assert.Catch<InvalidOperationException>(testDelegate);
        }
        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenAllParamertsAreCorrectAndPropertyTypeIsIntButPropertyValueIsBoolean_InvalidOperationExceptionExpacted()
        {
            //Arrange
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            //Тип свойства и тип переданного значения не совпадают
            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.IntProperty), default(bool), typeCondition));

            //Assert
            Assert.Catch<InvalidOperationException>(testDelegate);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenPropertyValueIsNull_InvalidOperationExceptionExpacted()
        {
            //Arrange
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            //Тип свойства и тип переданного значения не совпадают
            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), nameof(GetLambdaExpressionTestHelper.IntProperty), null, typeCondition));

            //Assert
            Assert.Catch<InvalidOperationException>(testDelegate);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenPropertyNameIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            //Тип свойства и тип переданного значения не совпадают

            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), null, default(Object), typeCondition));
            //Assert
            Assert.Catch<ArgumentNullException>(testDelegate);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenRecordDBTypeDoesNotContainPropertyCalledPropertyName_ArgumentExceptionExpacted()
        {
            //Arrange
            var propertyName = nameof(String.Length);
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act
            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(typeof(GetLambdaExpressionTestHelper), propertyName, default(int), typeCondition));

            //Assert
            Assert.Catch<ArgumentException>(testDelegate);
        }

        [Test]
        public void GetLambdaExpressionForPropertyValue_WhenRecordDBTypeIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act

            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetLambdaExpressionForPropertyValue(null, nameof(GetLambdaExpressionTestHelper.StringProperty), default(String), typeCondition));

            //Assert
            Assert.Catch<ArgumentNullException>(testDelegate);
        }

        [Test]
        public void GetMethodInfoForQueryableWhere_WhenGenericParametrTypeIsNotNull_MethodNameEqualsWhereExpacted()
        {
            //Arrange

            //Act

            var result = GenericMethodMaker.GetMethodInfoForQueryableWhere(typeof(UserBase));

            //Assert
            Assert.AreEqual("Where", result.Name);
        }

        [Test]
        public void GetMethodInfoForQueryableWhere_WhenGenericParametrTypeIsNull_ArgumentNullExceptionExpacted()
        {
            //Arrange

            //Act      
            var testDelegate = new TestDelegate(() => GenericMethodMaker.GetMethodInfoForQueryableWhere(null));

            //Assert
            Assert.Catch<ArgumentNullException>(testDelegate);
        }

        
        [Test]
        public void InvokeGenericQueryableMethodWithSelectorExpression_WhenAllParametrsAreCorrect_ResultAsIQueryableExpacted()
        {
            //Arrange
            var recordDBType = typeof(GetLambdaExpressionTestHelper);
            var propertyName = nameof(GetLambdaExpressionTestHelper.StringProperty);
            var propertyValue = default(string);
            var source = new List<GetLambdaExpressionTestHelper>();
            source.Add(new GetLambdaExpressionTestHelper());
            source.Add(new GetLambdaExpressionTestHelper());
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act      
            var result = GenericMethodMaker.InvokeGenericQueryableMethodWithSelectorExpression(recordDBType, propertyName, propertyValue, source.AsQueryable(), typeCondition);

            //Assert

            Assert.IsInstanceOf<IQueryable<GetLambdaExpressionTestHelper>>(result);
        }

        [Test]
        public void InvokeGenericQueryableMethodWithSelectorExpression_WhenSourceIsNull_TargetInvocationExceptionExpacted()
        {
            //Arrange

            var recordDBType = typeof(GetLambdaExpressionTestHelper);
            var propertyName = nameof(GetLambdaExpressionTestHelper.StringProperty);
            var propertyValue = default(string);
            var source = new List<GetLambdaExpressionTestHelper>();
            source.Add(new GetLambdaExpressionTestHelper());
            source.Add(new GetLambdaExpressionTestHelper());
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act      
            var testDelegate = new TestDelegate(() => GenericMethodMaker.InvokeGenericQueryableMethodWithSelectorExpression(recordDBType, propertyName, propertyValue, null, typeCondition) );

            //Assert
            Assert.Catch<System.Reflection.TargetInvocationException>(testDelegate);
        }

        [Test]
        public void InvokeGenericQueryableMethodWithSelectorExpression_WhenSourceTypeIsNotAppropriate_ArgumentExceptionExpacted()
        {
            //Arrange

            var recordDBType = typeof(GetLambdaExpressionTestHelper);
            var propertyName = nameof(GetLambdaExpressionTestHelper.StringProperty);
            var propertyValue = default(string);
            var source = new List<GetLambdaExpressionTestHelper>();
            source.Add(new GetLambdaExpressionTestHelper());
            source.Add(new GetLambdaExpressionTestHelper());
            var typeCondition = default(TypeFilterConditionForValueTypePropertyEnum);

            //Act      

            var testDelegate = new TestDelegate(() => GenericMethodMaker.InvokeGenericQueryableMethodWithSelectorExpression(recordDBType, propertyName, propertyValue, new object(), typeCondition));

            //Assert
            Assert.Catch<ArgumentException>(testDelegate);
        }

        
    }
        
}
