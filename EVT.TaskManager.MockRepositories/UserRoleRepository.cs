﻿using EVT.TaskManager.ModelEntities;
using EVT.TaskManager.Services;
using EVT.TaskManager.Specifications;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.MockRepositories
{
    public class UserRoleRepository : IRepository<UserRoleBase>
    {

        private readonly RoleManager<UserRoleBase> _roleManager;
        private async Task _AddRolesToStoreAsync(params UserRoleBase[] userRoleList)
        {
            if (userRoleList != null)
            {
                foreach (var role in userRoleList)
                {
                    var roleExist = await _roleManager.RoleExistsAsync(role.Name);
                    if (!roleExist)
                    {
                        var roleResult = await _roleManager.CreateAsync(role);
                        if (!roleResult.Succeeded)
                        {
                            throw new InvalidOperationException(String.Format("Role {0} wasn't added to role store", role.Name));
                        }
                    }
                }
            }
        }

        #region Ctor
        public UserRoleRepository(RoleManager<UserRoleBase> roleManager)
        {
            _roleManager = roleManager;

            var result = _AddRolesToStoreAsync(new UserRoleBase[] { new UserRoleBase("Admin"), new UserRoleBase("Manager"), UserRoleBase.DefaultUserRole });
        }
        #endregion

        #region CRUD
        public async Task<bool> AddAsync(UserRoleBase sourceEntity)
        {
            var result= await _roleManager.CreateAsync(sourceEntity);
            if (result.Succeeded)
                return true;
            else
                return false;
            
        }

        public async Task<bool> DeleteAsync(string id)
        {
            //return _userRoleList.Remove(_userRoleList.FirstOrDefault(i => i.Id == id));
            var roleResult = await _roleManager.FindByIdAsync(id);
            if (roleResult!=null)
            {
                var result =await _roleManager.DeleteAsync(roleResult);
                if (result.Succeeded)
                {
                    return true;
                }
            }
            return false;
        }

        public async Task<UserRoleBase> GetItemByIdAsync(string id)
        {
            //return _userRoleList.FirstOrDefault(it => it.Id == id);
            return await _roleManager.FindByIdAsync(id);
        }


        public async Task<bool> UpdateAsync(UserRoleBase sourceEntity)
        { 
            var roleResult = await _roleManager.FindByIdAsync(sourceEntity.Id);
            if (roleResult!=null)
            {
                if ((await _roleManager.UpdateAsync(sourceEntity)).Succeeded)
                    return true;
            }
            return false;

            /*
            updatedEntity = _userRoleList.FirstOrDefault(it => it.Id == sourceEntity.Id);
            if (updatedEntity != null)
            {
                updatedEntity.Name = sourceEntity.Name;

                return true;
            }
            */
            
        }

        #endregion

        public IEnumerable<UserRoleBase> FindItemsBySpec(ISpecification<UserRoleBase> spec)
        {
            var roles=_roleManager.Roles.Where(it => spec.IsSatisfiedBy(it));
            //var roles = _userRoleList.Where(it => spec.IsSatisfiedBy(it));
            return roles;
        }

        

        public IEnumerable<UserRoleBase> GetItems()
        {
            //return _userRoleList;
            return _roleManager.Roles;
        }

        
    }
}
