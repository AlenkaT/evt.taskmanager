﻿using EVT.TaskManager.ModelEntities;
using EVT.TaskManager.Services;
using EVT.TaskManager.Specifications;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EVT.TaskManager.MockRepositories
{
    public class MockUserRepository : IRepository<UserBase>, IActivator<UserBase>
    {
        //private readonly List<UserBase> _userList;
        private readonly IUpdater<UserBase> _userUpdater;
        private readonly UserManager<UserBase> _userManager;

        #region Ctor
        private async Task UserDBInitialize(IRepository<UserRoleBase> userRoleDB)
        {
            var userList= new List<UserBase>() 
            {
                new UserBase("FirstUs1", "user1@mail.ru", "123"),
                new UserBase("FirstUs2", "user2@mail.ru", "123"),
                new UserBase("FirstUs3", "user3@mail.ru", "123"),
                new UserBase("FirstUs4", "user4@mail.ru", "123"),
                new UserBase("FirstUs5", "user5@mail.ru", "123"),
            };

            var adminList = new List<UserBase>()
            {
                new UserBase("Admin", "admin@mail.ru", "321")
            };
           
            
            foreach (var user in adminList)
            {
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    //here we tie the new user to the role
                    await _userManager.AddToRoleAsync(user, "Admin");
                }
            }

            foreach (var user in userList)
            {
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    //here we tie the new user to the role
                    await _userManager.AddToRoleAsync(user, UserRoleBase.DefaultUserRole.Name);
                }
            }

            

        }
        public MockUserRepository(IRepository<UserRoleBase> userRoleDB, UserManager<UserBase> userManager)
        {

            _userManager = userManager;
           // _userList = new List<UserBase>();


            _userUpdater = new UserUpdater();
        }

        #endregion

        #region CRUD
        public async Task<UserBase> GetItemByIdAsync(string id)
        {
            // return _userList.FirstOrDefault(it => it.Id == id);
            return await _userManager.FindByIdAsync(id);
        }

        public async Task<bool> AddAsync(UserBase sourceUser)
        {
            /*var newUser = new UserBase(fullUserName: sourceUser.FullName, email: sourceUser.Email, accountName: sourceUser.AccountName);
            _userList.Add(newUser);
            return newUser;
            */
            var result = await _userManager.CreateAsync(sourceUser);
            if (result.Succeeded)
                return true;
            else
                return false;
        }

        public async Task<bool> UpdateAsync(UserBase sourceUser)
        {
            /*
            updatedUser = _userList.FirstOrDefault(it => it.Id == sourceUser.Id);
            if (updatedUser != null)
            {
                _userUpdater.Update(sourceUser, updatedUser);
                return true;
            }
            return false;
            */
            var roleResult = await _userManager.FindByIdAsync(sourceUser.Id);
            if (roleResult != null)
            {
                if ((await _userManager.UpdateAsync(sourceUser)).Succeeded)
                    return true;
            }
            return false;

        }

        public async Task<bool> DeleteAsync(string id)
        {
            //return _userList.Remove(_userList.FirstOrDefault(i => i.Id == id));
            var roleResult = await _userManager.FindByIdAsync(id);
            if (roleResult != null)
            {
                var result = await _userManager.DeleteAsync(roleResult);
                if (result.Succeeded)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        public void Activate(UserBase user)
        {
            /*
            var updater = _userUpdater as IActivator<UserBase>;
            if (updater != null && _userList.Contains(user)) updater.Activate(user);
            */
            throw new NotImplementedException();
        }

        public void Deactivate(UserBase user)
        {
            /*
            var updater = _userUpdater as IActivator<UserBase>;
            if (updater != null && _userList.Contains(user)) updater.Deactivate(user);
            */
            throw new NotImplementedException();
        }

        public IEnumerable<UserBase> FindItemsBySpec(ISpecification<UserBase> spec)
        {
            var users = _userManager.Users.Where(it => spec.IsSatisfiedBy(it));
            /*
            var users = _userList.Where(it => spec.IsSatisfiedBy(it));
            */
            return users;
        }

        public IEnumerable<UserBase> GetItems()
        {
            return _userManager.Users;
        }
    }
}
