﻿
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using EVT.TaskManager.Specifications;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.MockRepositories
{
    public class MockTaskRepository : MockRepositoryAbstract<TaskBase>
    {
        #region Private Fields
        private readonly ILogger<MockTaskRepository> _logger;
        #endregion

        #region Ctor
        public MockTaskRepository(ILogger<MockTaskRepository> logger) : base()
        {
            collection.AddRange(MockInitData.Instance.Tasks);
            _logger = logger;
        }
        #endregion

        #region Logging
        protected virtual void DoLogMethodResult(string methodName, params object?[] result)
        {
            if (_logger == null) return;
            var text = "Result of " + nameof(MockTaskRepository) + "." + methodName + "   " + String.Join(" ;", result); ;
            if (_logger.IsEnabled(LogLevel.Information)) _logger.LogInformation(text);
        }

        protected virtual void DoLogMethodResultWarn(string methodName, params object?[] result)
        {
            if (_logger == null) return;
            var text = "Result of " + nameof(MockTaskRepository) + "." + methodName + "   " + String.Join(" ;", result); ;
            if (_logger.IsEnabled(LogLevel.Warning)) _logger.LogInformation(text);
        }
        #endregion

        #region CRUD
        public override async Task<TaskBase> GetItemByIdAsync(string id)
        {
            var result = await base.GetItemByIdAsync(id);
            if (result != null)
            {
              //  result = result.CloneObject(new CloneMaker().Clone) as TaskBase; //toArray копирует объекты из одного массива в другой
            }

            DoLogMethodResult(nameof(GetItemByIdAsync), result);
            return result;
        }

        public override async Task AddAsync(TaskBase sourceTask)
        {
            var task = await GetItemByIdAsync(sourceTask.Id);
            if (task != null)
            {
                DoLogMethodResultWarn(nameof(AddAsync), "task was not added to repository");
            }
            else
            {
                collection.Add(sourceTask);
                DoLogMethodResult(nameof(AddAsync), "task was added to repository successfully");
            }
        }

        public override async Task UpdateAsync(TaskBase sourceTask)
        {
            var updatedTask = await GetItemByIdAsync(sourceTask.Id);
            if (updatedTask != null)
            {
                collection.Remove(updatedTask);
                collection.Add(sourceTask);
                DoLogMethodResult(nameof(DeleteAsync), "Task with id=" + sourceTask.Id + " was updated");
            }
            DoLogMethodResultWarn(nameof(DeleteAsync), "Task with id=" + sourceTask.Id + " was not found");
        }


        public override async Task DeleteAsync(string id)
        {
            var task = await GetItemByIdAsync(id);
            if (task != null)
            {
                await Task.Factory.StartNew(() => collection.Remove(task));

                DoLogMethodResult(nameof(DeleteAsync), "Task with id=" + id + " was deleted");

            }
            DoLogMethodResultWarn(nameof(DeleteAsync), "Task with id=" + id + " was not found");
        }
        #endregion

        public override Task<IEnumerable<TaskBase>> FindItemsBySpecAsync(ISpecification<TaskBase> spec)
        {
            var tasks = Task.FromResult(collection.Where(it => spec.IsSatisfiedBy(it)));
            DoLogMethodResult(nameof(FindItemsBySpecAsync), tasks);
            return tasks;
        }

        public override Task<IEnumerable<TaskBase>> GetItemsAsync()
        {
            var array = base.GetItemsAsync();
            DoLogMethodResult(nameof(GetItemsAsync), array);
            return array;
        }


    }
}
