﻿using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.Services;
using EVT.TaskManager.Specifications;

namespace EVT.TaskManager.MockRepositories
{
    public abstract class MockRepositoryAbstract<T> : IRepository<T> where T : class, IIdentity
    {

        protected readonly List<T> collection;
        public abstract Task AddAsync(T sourceEntity);

        public abstract Task DeleteAsync(string id);

        public abstract Task<IEnumerable<T>> FindItemsBySpecAsync(ISpecification<T> spec);

        public virtual Task<T> GetItemByIdAsync(string id) => Task.FromResult(collection.FirstOrDefault(x => x.Id == id));

        public virtual Task<IEnumerable<T>> GetItemsAsync() =>  Task.FromResult(collection.ToArray() as IEnumerable<T>);

        public abstract Task UpdateAsync(T sourceEntity);

        public MockRepositoryAbstract()
        {
            collection = new List<T>();     
        }
    }
}
