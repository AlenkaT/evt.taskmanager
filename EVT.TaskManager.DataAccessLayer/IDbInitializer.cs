﻿namespace EVT.TaskManager.DataAccessLayer
{
    public interface IDbInitializer
    {
        void Initialize();
        void SeedData();
    }
}
