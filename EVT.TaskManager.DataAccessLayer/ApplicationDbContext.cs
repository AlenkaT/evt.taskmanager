﻿
using EVT.TaskManager.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EVT.TaskManager.DataAccessLayer
{
    
    public class ApplicationDbContext : IdentityDbContext<UserBase, UserRoleBase, string>
    //public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<TaskBase>().ToTable("Tasks");
          
            base.OnModelCreating(builder);
        }
        
        
        

        public DbSet<TaskBase> Tasks { get; set; }

    }
}