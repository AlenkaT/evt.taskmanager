﻿using EVT.TaskManager.Entities;
using EVT.TaskManager.InitData;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EVT.TaskManager.DataAccessLayer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly IServiceScopeFactory _scopeFactory;
        private readonly UserManager<UserBase> _userManager;
       // private readonly UserFactoryBase _userFactory;



        public DbInitializer(IServiceScopeFactory scopeFactory, UserManager<UserBase> userManager)
        //public DbInitializer(IServiceScopeFactory scopeFactory, UserManager<IdentityUser> userManager)
        {
            _scopeFactory = scopeFactory;
            _userManager = userManager;
            //_userFactory=userFactory;
        }

        public void Initialize()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>())
                {
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                }
            }
        }

        public void SeedData()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<ApplicationDbContext>())
                {
                    var initData = MockInitData.Instance;
                    //add roles
                    if (!context.Roles.Any())
                    {
                        var roles = new List<UserRoleBase>()
                        {
                            UserRoleBase.AdminUserRole, new UserRoleBase("Manager"), UserRoleBase.DefaultUserRole
                        };

                        foreach (var role in roles)
                        {
                            context.Roles.Add(role);
                        }
                    }
                   
                    //add  users
                    if (!context.Users.Any())
                    {
                        foreach (var user in initData.Admins)
                        {
                            user.EmailConfirmed= true;
                            context.Users.Add(user);
                            var iur = new IdentityUserRole<string>();
                            iur.UserId = user.Id;
                            iur.RoleId = UserRoleBase.AdminUserRole.Id;
                            context.UserRoles.Add(iur);
                        }

                        foreach (var user in initData.Users)
                        {
                            user.EmailConfirmed = true;
                            context.Users.Add(user);
                            var iur = new IdentityUserRole<string>();
                            iur.UserId = user.Id;
                            iur.RoleId = UserRoleBase.DefaultUserRole.Id;
                            context.UserRoles.Add(iur);
                        }
                      
                    }

                    if (!context.Tasks.Any())
                    {
                        foreach (var task in initData.Tasks)
                        {
                            context.Tasks.Add(task);
                        }
                    }

                    context.SaveChanges();
                }
            }
        }
    }
}
