﻿
using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.DataObjectInterfaces;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Services;
using Microsoft.Extensions.Logging;
using EVT.TaskManager.Services.Interfaces;
using EVT.TaskManager.Common.Extensions;

namespace EVT.TaskManager.DataObjects
{
    public class DBManager : DataObjectInterfaces.IDBManager
    {
        #region Private Fields

        private readonly IRepository<TaskBase> _taskDB;
       
        private readonly ILogger<DBManager> _logger;

        #endregion

        #region Ctor
        //public Facade(IRepository<UserBase> userDB, IRepository<UserRoleBase> userRoleDB, IRepository<TaskBase> taskDB, IActivator<UserBase> userActivator)
        public DBManager(ILogger<DBManager> logger, IRepository<TaskBase> taskDB)

        {
            _taskDB = taskDB;
            _logger = logger;

        }
        #endregion

        #region Protected Methods
        protected virtual IRepository<TEntity> _RepositorySearch<TEntity>() where TEntity : class, IIdentity
        {
            var fieldInfos = this.GetType().GetFields(System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.DeclaredOnly);
            if (fieldInfos == null) return null;
            foreach (var fieldInfo in fieldInfos)
            {
                if (fieldInfo.FieldType.IsAssignableFrom(typeof(IRepository<TEntity>)))
                {
                    var value = fieldInfo.GetValue(this);
                    if (value != null)
                    {
                        return value as IRepository<TEntity>;
                    }
                }
            }
            _logger.WarnLogMethodResultToFileUTF8(this, "", nameof(_RepositorySearch), "Repository type of " + typeof(TEntity).Name + " was not found");
            return null;
        }
        #endregion


        #region NVI implementation
        protected async virtual Task<TEntity> DoGetEntityByIdAsync<TEntity>(string id) where TEntity : class, IIdentity
        {
            TEntity result = null;
            var repository = _RepositorySearch<TEntity>();
            if (repository != null)
            {
                result = await repository.GetItemByIdAsync(id);
               
               //DoLogMethodResult(nameof(DoGetEntityByIdAsync), "Item was got successfully");
            }
            _logger.LogMethodResultToFileUTF8(this, "", nameof(DoGetEntityByIdAsync), result);
            return result;
        }

        protected async virtual Task DoAddEntityAsync<TEntity>(TEntity item) where TEntity : class, IIdentity
        {
            var repository = _RepositorySearch<TEntity>();
            if (repository != null)
            {
                await repository.AddAsync(item);
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoAddEntityAsync), "Entity of type "+typeof(TEntity)+" was added to repository successfully");
            }
        }

        protected async virtual Task DoDeleteEntityByIdAsync<TEntity>(string id) where TEntity : class, IIdentity
        {
            var repository = _RepositorySearch<TEntity>();         
            {
                await repository.DeleteAsync(id);
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoDeleteEntityByIdAsync), "Entity of type " + typeof(TEntity) + " was deleted from repository successfully");
            }
            
        }

        protected async virtual Task DoUpdateEntityAsync<TEntity>(TEntity item) where TEntity : class, IIdentity
        {
            var repository = _RepositorySearch<TEntity>();
            if (repository == null)
            {
                await repository.UpdateAsync(item);
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateEntityAsync), "Entity of type " + typeof(TEntity) + " was updated in repository successfully");
            }
        }

        protected async virtual Task<IQueryable<TEntity>> DoGetEntitiesAsync<TEntity>(int startIndex = 0, int? count = null) where TEntity : class, IIdentity
        {
            var repository = _RepositorySearch<TEntity>();
            IQueryable<TEntity> result = null;
            if (repository != null)
            {
                var items=await repository.GetItemsAsync();
                
                var endIndex = items.Count() - startIndex;
                result =  items.Skip(startIndex).Take(count?? endIndex);
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoGetEntitiesAsync), "Entities of type " + typeof(TEntity) + " from index "+startIndex+" to index: "+endIndex+" was returned (count="+(endIndex-startIndex).ToString()+")");
            }
            return result;
        }


        protected virtual IEnumerable<TEntity> DoFindEntities<TEntity>(Specifications.ISpecification<TEntity> spec) where TEntity : class, IIdentity
        {
            throw new NotImplementedException();
            /*
            var entities = DoGetEntities<TEntity>();
            var list=new List<TEntity>();
            foreach (var entity in entities)
            {
                if (spec.IsSatisfiedBy(entity)) list.Add(entity);
            }
            return list;
            */
        }


        #endregion

        #region IDBManager implementaion
        public async Task<TEntity> GetEntityByIdAsync<TEntity>(string id) where TEntity : class, IIdentity
        {
            return await DoGetEntityByIdAsync<TEntity>(id);
        }

        public async Task AddEntityAsync<TEntity>(TEntity item) where TEntity : class, IIdentity
        {
            await DoAddEntityAsync<TEntity>(item);
        }

        public async Task UpdateEntityAsync<TEntity>(TEntity item) where TEntity : class, IIdentity
        { 
             await DoUpdateEntityAsync<TEntity>(item);
        }
        public async Task DeleteEntityByIdAsync<TEntity>(string id) where TEntity : class, IIdentity
        {
            await DoDeleteEntityByIdAsync<TEntity>(id);
        }

        public async Task<IQueryable<TEntity>> GetEntitiesAsync<TEntity>(int startIndex = 0, int? count = null) where TEntity : class, IIdentity
        {
            return await DoGetEntitiesAsync<TEntity>(startIndex, count);
        }

        public IEnumerable<TEntity> FindEntities<TEntity>(Specifications.ISpecification<TEntity> spec) where TEntity : class, IIdentity
        {
            return DoFindEntities<TEntity>(spec);
        }
       


        #endregion

        
    }
}