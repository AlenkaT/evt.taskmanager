﻿using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Entities.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace EVT.TaskManager.Entities
{
    public class Department : IIdentity
    {
        [Required]
        [MaxLength(length: 40)]
        public string Id { get; private set; } //совсем без свойства при выполнении update-database ошибки 
        
        [Required]
        [MaxLength(length: 100)]
        public string Name { get; internal set; }

        public Department()
        {
            Id = Guid.NewGuid().ToString();
            Name = "";
        }

        public override string ToString()
        {
            return this.PrintProperties();
        }
    }
}