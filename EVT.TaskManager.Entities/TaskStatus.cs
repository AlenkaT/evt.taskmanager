﻿using EVT.TaskManager.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EVT.TaskManager.Entities
{
    public class TaskStatus
    {
        
        public readonly static TaskStatus DefaultStatus = new TaskStatus(null);
        private readonly List<string> _statusList = new List<string>();
        private string _currentStatus;
        public string CurrentStatus => _currentStatus;

        #region Ctor
        public TaskStatus(ICollection<string> statusList)
        {
            if (statusList != null)
            {
                _statusList.AddRange(statusList);
            }
            else { _statusList.AddRange(new[] { "new", "inProgress", "done" }); }
            _currentStatus = _statusList[0];
        }
        #endregion

        public string GetStartStatus()
        {
            return _statusList[0];
        }

        public void SetStartStatus()
        {
            _currentStatus = _statusList[0];
        }

        public void SetNextStatus()
        {
            if (_currentStatus != _statusList.Last())
            {
                _currentStatus = _statusList.ElementAt(_statusList.IndexOf(_currentStatus) + 1);
            }
        }

        public Boolean TrySetStatus(string nextStatus)
        {
            if (!_statusList.Contains(nextStatus))
            { return false; }
            else
            {
                _currentStatus = nextStatus;
                return true;
            }
        }

        public override string ToString()
        {
            return this.PrintProperties();
        }
    }


}
