﻿using Microsoft.AspNetCore.Identity;
using System.Reflection;
using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.Common.Extensions;

namespace EVT.TaskManager.Entities
{
    public class UserRoleBase: IdentityRole<string>, IIdentity
    {
        public static UserRoleBase DefaultUserRole = new UserRoleBase("User");
        public static UserRoleBase AdminUserRole = new UserRoleBase("Admin");

        public override string Id
        { get => base.Id; set => throw new AccessViolationException("This property is readonly"); }
        

        #region Ctor
        public UserRoleBase(string name):base(name)
        {
            base.Id = Guid.NewGuid().ToString();
        }
        #endregion

        public override string ToString()
        {
            return this.PrintProperties();
        }
    }
}