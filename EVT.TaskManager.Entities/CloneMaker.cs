﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Entities
{
    public class CloneMaker
    {
        public object CloneByCloneableFields(object source)
        {
            var result = this.MemberwiseClone();
            var fields=result.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach(var field in fields)
            {
                if (typeof(IEntityCloneable).IsAssignableFrom(field.FieldType))
                {
                    var newvalue = field.GetValue(this) as ICloneable;
                    if (newvalue != null)
                    {
                        field.SetValue(result, newvalue.Clone()) ;
                    }
                }
            }
           
            return result;
        }
    }
}
