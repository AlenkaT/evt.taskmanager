﻿
using EVT.TaskManager.Entities;
using EVT.TaskManager.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Specifications
{
    public sealed class TasksForUserIdSpec : SpecificationBase<TaskBase>
    {
        public TasksForUserIdSpec(String userId) : base(task => task.Handler.Id == userId)
        {
        }

       
    }
}
