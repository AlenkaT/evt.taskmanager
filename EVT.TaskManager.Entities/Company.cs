﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Entities.Interfaces;

namespace EVT.TaskManager.Entities
{
    public class Company : IIdentity
    {
        [Required]
        [MaxLength(length: 40)]
        public string Id { get; private set; }

        [Required]
        [MaxLength(length: 100)]
        public string Name { get; internal set; }
        public ICollection<Department> Departments { get; internal set; }

        public Company()
        {
            Id= Guid.NewGuid().ToString();
            Name = "";
            Departments = new List<Department>();
        }

        public override string ToString()
        {
            return this.PrintProperties();
        }

    }
}
