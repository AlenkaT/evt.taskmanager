﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Runtime.CompilerServices;
using Microsoft.AspNetCore.Identity;
using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.Common.Extensions;

namespace EVT.TaskManager.Entities
{

    public class UserBase : IdentityUser<string>, IIdentity
    {

        #region Derrived Properties

        [PersonalData]
        public override string Id
        { get => base.Id; set => throw new AccessViolationException("This property is readonly"); }


        [ProtectedPersonalData]
        public override string UserName
        {
            get => base.UserName;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.UserName = value;
            }
        }


        //     Gets or sets the normalized user name for this user.
        public override string NormalizedUserName
        {
            get => base.NormalizedUserName;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.NormalizedUserName = value;
            }
        }
        //     Gets or sets the email address for this user.

        [ProtectedPersonalData]
        public override string Email
        {
            get => base.Email;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.Email = value;
            }
        }



        //     Gets or sets the normalized email address for this user.
        public override string NormalizedEmail
        {
            get => base.NormalizedEmail;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.NormalizedEmail = value;
            }
        }

        /*
           //     Gets or sets a flag indicating if a user has confirmed their email address.        
           //     True if the email address has been confirmed, otherwise false.
           [PersonalData]
           public virtual bool EmailConfirmed {get;set;}

       */

        //     Gets or sets a salted and hashed representation of the password for this user.
        public override string PasswordHash
        {
            get => base.PasswordHash;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.PasswordHash = value;
            }
        } //Свойство устанавливается системой Identity при использовании стандартной версии
        private void SetPasswordHash(string password)
        {
            var hasher = new PasswordHasher<UserBase>();
            base.PasswordHash = hasher.HashPassword(this, password);
        }

        /*
            //     A random value that must change whenever a users credentials change (password
            //     changed, login removed)
            public virtual string SecurityStamp {get;set;} 

            //     A random value that must change whenever a user is persisted to the store
            public virtual string ConcurrencyStamp {get;set;} = Guid.NewGuid().ToString();

        */
        //     Gets or sets a telephone number for the user.
        [ProtectedPersonalData]
        public override string PhoneNumber
        {
            get => base.PhoneNumber;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.PhoneNumber = value;
            }
        }

        //     Gets or sets a flag indicating if a user has confirmed their telephone address.
        //     True if the telephone number has been confirmed, otherwise false.
        /*
        [PersonalData]
        public virtual bool PhoneNumberConfirmed { get; set; }

        //     Gets or sets a flag indicating if two factor authentication is enabled for this   user.
        //     True if 2fa is enabled, otherwise false.
        [PersonalData]
        public virtual bool TwoFactorEnabled { get; set; }

        //     Gets or sets the date and time, in UTC, when any user lockout ends.
        //     A value in the past means the user is not locked out.
        public virtual DateTimeOffset? LockoutEnd { get; set; }

        //     Gets or sets a flag indicating if the user could be locked out.
        //     True if the user could be locked out, otherwise false.
        public virtual bool LockoutEnabled { get; set; }
        //     Gets or sets the number of failed login attempts for the current user.
        */
        public override int AccessFailedCount
        {
            get => base.AccessFailedCount;
            set
            {
                if (!Assembly.GetCallingAssembly().GetName().FullName.StartsWith("Microsoft.Extensions.Identity"))
                {
                    throw new AccessViolationException("This property is readonly");
                }
                base.AccessFailedCount = value;
            }

        }

        //     Initializes a new instance of Microsoft.AspNetCore.Identity.IdentityUser`1.


        #endregion

        [ProtectedPersonalData]
        [Required]
        [MaxLength(length: 250)]
        public string FullName { get; internal set; } //Фио
        
        [Required]
        public DateTime RegistrationDate { get; }

        [ProtectedPersonalData]
        public bool IsActive { get; internal set; }

        // public UserBase? Manager { get; internal set; }
        // public string? ManagerId { get; set; }
        public string? DepartmentId { get; internal set; }
        public Department Department { get; internal set; }
        public string? CompanyId { get; internal set; }
        public Company? Company { get; internal set; }


        #region C-tor

        /// <summary>
        /// Для использования с последующим добавлением в бд через UserManager
        /// </summary>
        /// <param name="fullUserName"></param>
        /// <param name="email"></param>
        /// <param name="accountName"></param>
        internal UserBase(string fullUserName, string email, string accountName) : base()
        {
            base.Id = Guid.NewGuid().ToString();
            base.PhoneNumber = "";
            IsActive = true;
            FullName = fullUserName;
            RegistrationDate = DateTime.UtcNow;
            DepartmentId = null;
            CompanyId = null;
            Company = null;
            //ManagerId = null;
            base.Email = email;
            var userName = accountName == "" ? email.IndexOf("@") < 0 ? email : email.Remove(email.IndexOf("@")) : accountName;
            base.UserName = userName;
        }

        //для предварительно инициализации бд
        internal UserBase(string fullUserName, string email, string accountName, string password) : this(fullUserName, email, accountName)
        {
            base.NormalizedEmail = email.ToUpper();
            SetPasswordHash(password);
            base.NormalizedUserName = UserName.ToUpper();
            base.SecurityStamp = Guid.NewGuid().ToString();
        }

        // При отсутствии конструктора без полей EF ругается, что нет подходящего конструктора
        public UserBase() : this("Full Name", "login@server.com", "login")
        {
        }

        #endregion


        public override string ToString()
        {
            return this.PrintProperties();
        }

        /*
        internal virtual void AddChild(UserBase user)
        {
            if (user != null) _children.Add(user.Id);
        }

        internal virtual void DeleteChild(UserBase user)
        {
            if (_children.Contains(user.Id)) { _children.Remove(user.Id); }
        }

       */
    }

}
