﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("EVT.TaskManager.UnitTests")]
[assembly: InternalsVisibleToAttribute("EVT.TaskManager.Services")]
[assembly: InternalsVisibleToAttribute("EVT.TaskManager.InitData")]
