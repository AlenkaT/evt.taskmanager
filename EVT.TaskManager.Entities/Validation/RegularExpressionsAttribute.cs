﻿using System;

namespace System.ComponentModel.DataAnnotations
{
    public class RegularExpressionsAttribute : ValidationAttribute
    {
        private readonly List<RegularExpressionAttribute> _attributes;
   
        public RegularExpressionsAttribute(string[] patterns):base("")
        {
            _attributes = new List<RegularExpressionAttribute>();
            foreach (string pattern in patterns)
            {
                _attributes.Add(new RegularExpressionAttribute(pattern));
            }
        }

        public override bool IsValid(object value)
        {
            foreach (RegularExpressionAttribute attribute in _attributes)
            {
                if (!attribute.IsValid(value)) return false;
            }
            return true;
        }
    }
}