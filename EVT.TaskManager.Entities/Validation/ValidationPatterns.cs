﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Entities
{
    public static class ValidationPatterns
    {
        /*
        public const string EmailPattern =
           @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
         @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
         @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
        */
        public const string EmailPattern = @"[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}";

        public const string PasswordSymLowerPattern = @"^([a - z]+)$";
        public const string PasswordSymUpperPattern = @"([A - Z]+)";
        public const string PasswordSymDigitPattern = @"(\d+)";
        public const string PasswordPattern= @"^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])$";
    }

}
