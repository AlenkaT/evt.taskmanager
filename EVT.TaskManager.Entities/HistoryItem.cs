﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Entities.Interfaces;

namespace EVT.TaskManager.Entities
{
    public class HistoryItem : IIdentity
    {
        [Required]
        [MaxLength(length: 40)]
        public string Id { get; }

        [Required]
        [MaxLength(length: 40)]
        public string Title { get; }

        [Required]
        [MaxLength(length: 40)]
        public string SubmitterId { get; }
        public UserBase Submitter { get; }

        [Required]
        public DateTime CreationDate { get; }

        [MaxLength(length: 40)]
        public string ParentTaskId { get; }
        public TaskBase ParentTask { get; set; }

        private string _text;
        public string Text { get => _text; internal set { _text = value; _SetUpdateDate(); } }
        
        [Required]
        public DateTime UpdateDate { get; private set; }


        #region C-tor
        public HistoryItem(string  title, string text, UserBase submitter, TaskBase sourceTask)
        {
            Id= Guid.NewGuid().ToString();  
            Title = title;
            Text = text;
            CreationDate = DateTime.UtcNow;
            SubmitterId = submitter.Id;
            UpdateDate = CreationDate;
            ParentTaskId = sourceTask.Id;
        }

        public HistoryItem(UserBase submitter, TaskBase parentTask):this("Action","", submitter, parentTask)
        {
        }
        #endregion

        protected virtual void _SetUpdateDate()
        {
            UpdateDate = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return this.PrintProperties();
        }

    }
}
