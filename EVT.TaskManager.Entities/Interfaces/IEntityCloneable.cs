﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Entities
{
    public interface IEntityCloneable
    {
        object CloneObject();
    }
}
