﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Entities.Interfaces;

namespace EVT.TaskManager.Entities
{
    /*Задача включает в себя
- Название;
- Описание;
- Дата создания;
- Дата/время закрытия задачи;
- Комментарии;
- Статус(достаточно будет 3 статусов: Создана, В работе, Завершена);
- Ориентировочная дата/время завершения*;
- Фактическое время на выполнение задачи*;
    */
    public class TaskBase : IIdentity
    {
        [Required]
        [MaxLength(length: 40)]
        public string Id { get; private set; }

        private string _title;

        [Required]
        [MaxLength(length: 200)]
        public string Title { get=>_title; set { _title = value; _SetUpdateDate(); } }
        
        private string? _description;
        public string? Description { get=>_description; set { _description = value; _SetUpdateDate(); } }

        
        private TaskBase? _parentTask;
        public TaskBase? ParentTask { get=>_parentTask; set { _parentTask = value; _SetUpdateDate(); } }

        
        private List<TaskBase> _children;
        public IEnumerable<TaskBase> Children { get => _children; }

        [Required]
        [MaxLength(length: 40)]
        public string SubmitterId { get; }
        public UserBase Submitter { get; }

        
        private UserBase? _handler;
        public UserBase? Handler { get=>_handler; set { _handler = value; _SetUpdateDate(); } }
       
        public TaskStatus Status { get; }
        
        [Required]
        public DateTime CreationDate { get;  }
        
        [Required]
        public DateTime UpdateDate { get; private set; }

        private DateTime? _deadlineDate;
        public DateTime? DeadlineDate { get=>_deadlineDate; set { _deadlineDate = value; _SetUpdateDate(); } }
        
        private DateTime? _closeDate;
        public DateTime? CloseDate { get=>_closeDate; set { _closeDate = value; _SetUpdateDate(); } }      

        #region C-tor
        internal TaskBase(string title, UserBase submitter, string? description, UserBase? handler, DateTime? deadlineDate, TaskStatus status=null)
        {
            Id = Guid.NewGuid().ToString();
            Title = title;
            Description= description;
            Handler = handler;
            CreationDate = DateTime.UtcNow;
            DeadlineDate = deadlineDate;
            CloseDate = null;
            Submitter = submitter;
            UpdateDate = CreationDate;
         //   _comments = new List<Operation>();
         //   _children = new List<TaskBase>();
         //   _history=new List<Operation>();
            if (status != null)
            {
                Status = status;
            }
            else 
            {
                Status = TaskStatus.DefaultStatus;
            }
        }

        public TaskBase()
        {

        }

        #endregion

        protected virtual void _SetUpdateDate()
        {
            UpdateDate = DateTime.UtcNow;
        }

        public override string ToString()
        {
            return this.PrintProperties();
        }

        //TODO 
        /*
        internal virtual void AddChild(TaskBase task)
        {
            if (task != null) _children.Add(task);
        }

        internal virtual void DeleteChild(TaskBase task)
        {
            if (_children.Contains(task)) { _children.Remove(task); }
        }

        internal virtual void AddComment(string text, UserBase submitter)
        {
            _comments.Add(new Comment("Comment", text, submitter, this));
        }

        internal virtual void DeleteComment(string text)
        {
            _comments.Remove(_comments.FirstOrDefault(it=>it.Text==text));
        }

        internal virtual void AddActionToHistory(string title, string text, UserBase submitter)
        {
            _history.Add(new Comment(title, text, submitter, this));
        }

        internal virtual void DeleteActionFromHistory(string text)
        {
            throw new NotImplementedException();
        }
        */
    }
}
