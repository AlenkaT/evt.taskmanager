﻿using Microsoft.Extensions.Logging;

namespace EVT.Logging
{
    public class Logger<T>: ILogger<T>
    {

        private readonly FileInfo _fileInfo;
        #region Ctor
        public Logger(FileInfo fileInfo)
        {
            _fileInfo = fileInfo;
        }
        #endregion

        #region Implement interface
        public IDisposable BeginScope<TState>(TState state)
        {
            
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception? exception, Func<TState, Exception?, string> formatter)
        {
            throw new NotImplementedException();
        }
        #endregion

        protected virtual void DoLogMethodResult(string methodName, params object?[] result)
        {
            if (_logger == null) return;
            var text = "Result of " + nameof(TaskBaseManager<TaskBase>) + "." + methodName + "   " + String.Join(" ;", result);
            if (_logger.IsEnabled(LogLevel.Information)) _logger.LogInformation(text);
        }

        protected virtual void DoLogMethodResultWarn(string methodName, params object?[] result)
        {
            if (_logger == null) return;
            var text = "Result of " + nameof(TaskBaseManager<TaskBase>) + "." + methodName + "   " + String.Join(" ;", result);
            if (_logger.IsEnabled(LogLevel.Warning)) _logger.LogInformation(text);
        }

    }
}