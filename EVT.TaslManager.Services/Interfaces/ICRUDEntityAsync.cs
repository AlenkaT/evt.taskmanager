﻿using EVT.TaskManager.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Services.Interfaces
{
    public interface ICRUDEntityAsync<TEntity> where TEntity : class, IIdentity
    {
        ValueTask<TEntity> GetItemByIdAsync(string id);
        Task UpdateAsync(TEntity sourceEntity);
        Task DeleteAsync(string id);
        Task AddAsync(TEntity sourceEntity);
    }
}
