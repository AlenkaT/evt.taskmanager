﻿using EVT.TaskManager.Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Services.Interfaces
{
    public interface ICRUDEntity<TEntity> where TEntity : class, IIdentity
    {
        TEntity GetItemById(string id);
        bool Update(TEntity sourceEntity);
        bool Delete(string id);
        bool Add(TEntity sourceEntity);
    }
}
