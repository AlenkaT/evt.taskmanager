﻿using EVT.TaskManager.Entities.Interfaces;
using EVT.TaskManager.Specifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Services.Interfaces
{
    public interface IRepository<TEntity>: ICRUDEntityAsync<TEntity> where TEntity :  class, IIdentity
    { 
        Task<IQueryable<TEntity>> GetItemsAsync();
        Task<IQueryable<TEntity>> FindItemsBySpecAsync(ISpecification <TEntity> spec);

    }
}

