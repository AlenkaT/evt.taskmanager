﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EVT.TaskManager.DataAccessLayer;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Services.Interfaces;
using EVT.TaskManager.Specifications;
using Microsoft.Extensions.Logging;

namespace EVT.TaskManager.Services.Repositories
{
    public class TaskBaseRepository : IRepository<TaskBase>
    {
        private readonly ApplicationDbContext _context;
        ILogger<TaskBaseRepository> _logger;

        #region Ctor
        public TaskBaseRepository(ILogger<TaskBaseRepository> logger, ApplicationDbContext context)
        {
            _context = context;
            _logger = logger;
        }
        #endregion

        public async Task AddAsync(TaskBase sourceEntity)
        {
            try
            {
                await _context.Tasks.AddAsync(sourceEntity);
                await _context.SaveChangesAsync();
                _logger.LogMethodResultToFileUTF8(this, "", nameof(AddAsync), "Entity was added to database successfully");
            }
            catch (Exception ex)
            {
                _logger.CriticalLogMethodResultToFileUTF8(this, "", nameof(AddAsync), "There was thrown Exception: " + ex.PrintProperties());
                throw;
            }
        }

        public async Task DeleteAsync(string id)
        {
            try
            {
                var entity = await GetItemByIdAsync(id);
                if (entity!=null)
                {
                    _context.Tasks.Remove(entity);
                }
                await _context.SaveChangesAsync();
                _logger.LogMethodResultToFileUTF8(this, "", nameof(AddAsync), "Entity was deleted successfully");
            }
            catch (Exception ex)
            {
                _logger.CriticalLogMethodResultToFileUTF8(this, "", nameof(DeleteAsync), "There was thrown Exception: " + ex.PrintProperties());
                throw;
            }
        }

        public Task<IQueryable<TaskBase>> FindItemsBySpecAsync(ISpecification<TaskBase> spec)
        {
            throw new NotImplementedException();
        }

        public async ValueTask<TaskBase> GetItemByIdAsync(string id)
        {
            var result = await _context.Tasks.FindAsync(id);
            if (result != null)
            {
                _logger.LogMethodResultToFileUTF8(this, "", nameof(GetItemByIdAsync), "Entity was returned successfully " + result.PrintProperties());
            }
            else
            {
                _logger.LogMethodResultToFileUTF8(this, "", nameof(GetItemByIdAsync), "Entity with id:"+id+" was not found");
            }
            return result;
        }

        public Task<IQueryable<TaskBase>> GetItemsAsync()
        {
            var result = _context.Tasks;
            if (!result.Any())
            {
                _logger.LogMethodResultToFileUTF8(this, "", nameof(GetItemsAsync), "Entities were returned successfully " + result.PrintProperties());
            }
            else
            {
                _logger.LogMethodResultToFileUTF8(this, "", nameof(GetItemByIdAsync), "Repository does not contain any elements");
            }
            return Task.FromResult(result.AsQueryable());
        }

        public async Task UpdateAsync(TaskBase sourceEntity)
        {
            try
            {
                var entity = await GetItemByIdAsync(sourceEntity.Id);
                if (entity != null)
                {
                    _context.Tasks.Update(entity);
                }
                await _context.SaveChangesAsync();
                _logger.LogMethodResultToFileUTF8(this, "", nameof(UpdateAsync), "Entity was updated successfully");
            }
            catch (Exception ex)
            {
                _logger.CriticalLogMethodResultToFileUTF8(this, "", nameof(UpdateAsync), "There was thrown Exception: " + ex.PrintProperties());
                throw;
            }
        }
    }
}
