﻿
using EVT.TaskManager.Common.Extensions;
using EVT.TaskManager.Entities;
using EVT.TaskManager.Entities.Interfaces;

using EVT.TaskManager.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;



namespace EVT.TaskManager.Services
{
    /// <summary>
    /// Выполняет изменение задачи в репозитории
    /// </summary>
    /// <typeparam name="TTask"></typeparam>
    public class TaskBaseManager<TTask> where TTask : TaskBase
    {
        #region Private Fields
        private readonly IRepository<TTask> _taskRepository;
        private readonly UserManager<UserBase> _userManager;

        private readonly ILogger<TaskBaseManager<TTask>> _logger;
        #endregion

        #region Ctor
        public TaskBaseManager(ILogger<TaskBaseManager<TTask>> logger, IRepository<TTask> taskRepository, UserManager<UserBase> userManager)
        {
            _taskRepository = taskRepository;
            _userManager = userManager;
            _logger = logger;
        }
        #endregion


        #region Protected virtual Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="taskId"></param>
        /// <param name="propertyName">Зарезервировано для будующих времен</param>
        /// <returns></returns>
        protected virtual async Task<TTask> DoGetTaskFromRepositoryForUserAsync(String userName, string taskId)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user != null)
            {
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoGetTaskFromRepositoryForUserAsync), "User was found");

                //TODO
                /*
                if (!user.CanEditProperty(propertyName))
                {
                    DoLogMethodResultWarn(nameof(DoGetTaskFromRepositoryForUserAsync), "access denied");
                }
                else
                {
                */

                var task = await _taskRepository.GetItemByIdAsync(taskId);
                if (task != null)
                {
                    _logger.LogMethodResultToFileUTF8(this, "", nameof(DoGetTaskFromRepositoryForUserAsync), "Task with id:" + taskId + " was found");
                    return task;
                }
                else
                {
                    _logger.WarnLogMethodResultToFileUTF8(this, "", nameof(DoGetTaskFromRepositoryForUserAsync), "Task with id:" + taskId + " was not found");
                }
            }
            else
            {
                _logger.WarnLogMethodResultToFileUTF8(this, "", nameof(DoGetTaskFromRepositoryForUserAsync), "User :" + userName + " was not found");
            }
            return null;

        }

      
        protected virtual void DoUpdateTitle(TaskBase source, string title)
        {
            if (source.Title != title)
            {
                source.Title = title;
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateTitle), "Task.Title was updated successfully");
            }

        }

        protected virtual void DoUpdateDescription(TaskBase source, string? description)
        {
            if (source.Description != description)
            {
                source.Description = description;
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateDescription), "Task.Description was updated successfully");
            }
        }

        protected async virtual Task DoUpdateHandlerAsync(TaskBase source, string handlerUserName)
        {
            if (source.Handler.UserName != handlerUserName)
            {
                var handler = await _userManager.FindByNameAsync(handlerUserName);
                if (handler == null)
                {
                    _logger.WarnLogMethodResultToFileUTF8(this, "", nameof(DoUpdateHandlerAsync), "User: " + handlerUserName + " was not found");
                }
                else
                {
                    source.Handler = handler;
                    _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateHandlerAsync), "Task.Handler was updated successfully");
                }
            }
        }

        protected async virtual Task DoUpdateParentTaskAsync(TaskBase source, string parentTaskId)
        {
            if (source.ParentTask.Id != parentTaskId)
            {
                var parentTask = await _taskRepository.GetItemByIdAsync(parentTaskId);
                if (parentTask == null)
                {
                    _logger.WarnLogMethodResultToFileUTF8(this, "", nameof(DoUpdateParentTaskAsync), "Task with id: " + parentTaskId + " was not found");
                }
                else
                {
                    source.ParentTask = parentTask;
                    _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateParentTaskAsync), "Task.ParentTask was updated successfully");
                }
            }

        }


        protected virtual void DoUpdateCloseDate(TaskBase source, DateTime? closeDate)
        {
            if (source.CloseDate != closeDate)
            {
                source.CloseDate = closeDate;
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateCloseDate), "Task.CloseDate was updated successfully");
            }
        }

        protected virtual void DoUpdateDeadlineDate(TaskBase source, DateTime? deadlineDate)
        {
            if (source.DeadlineDate != deadlineDate)
            {
                source.DeadlineDate = deadlineDate;
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateDeadlineDate), "Task.DeadlineDate was updated successfully");
            }
        }

        protected virtual void DoUpdateStatus(TaskBase source, string newStatus)
        {
            if (source.Status.CurrentStatus != newStatus)
            {
                source.Status.TrySetStatus(newStatus);
                _logger.LogMethodResultToFileUTF8(this, "", nameof(DoUpdateStatus), "Task.Status was updated successfully");
            }
        }

        //TODO
        //var text = String.Format("Обновлено значение свойства {0} ({1}=>{2})", propertyName, oldValue, newValue);
        //task.AddActionToHistory("Updated by" + user.FullName, text, user);

        #endregion

        public async Task<Boolean> TryUpdateAsync(String userNameFromContext, TaskBase source)
        {
            if (source == null || userNameFromContext == null) return false;
            var task = await DoGetTaskFromRepositoryForUserAsync(userNameFromContext, source.Id);
            if (task == null) return false;
            DoUpdateTitle(task, source.Title);
            DoUpdateDescription(task, source.Description);
            DoUpdateDeadlineDate(task, source.DeadlineDate);
            DoUpdateCloseDate(task, source.CloseDate);
            if (source.Handler!=null) await DoUpdateHandlerAsync(task, source.Handler.Id);
            if (source.ParentTask != null) await DoUpdateParentTaskAsync(task, source.ParentTask.Id);
            DoUpdateStatus(task, source.Status.CurrentStatus);
            await _taskRepository.UpdateAsync(task);
            return true;
        }


    }
}
