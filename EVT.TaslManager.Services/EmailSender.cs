﻿using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Logging;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Services
{
    public class EmailSender : IEmailSender
    {
        private readonly ILogger<EmailSender> _logger;

        public EmailSender(ILogger<EmailSender> logger)
        {
            _logger = logger;
        }
        public async Task SendEmailAsync(string recipient, string subject, string htmlMessage)
        {
            MimeMessage mail = new MimeMessage();
            mail.From.Add(new MailboxAddress(Process.GetCurrentProcess().ProcessName, "evt.taskmanager@mail.ru"));
            mail.To.Add(new MailboxAddress("",recipient));
            mail.Subject = subject;
            
            mail.Body = new BodyBuilder() { HtmlBody = htmlMessage }.ToMessageBody();
            using (SmtpClient client = new SmtpClient())
            {
                try
                {
                    client.Connect("smtp.mail.ru", 465, true);
                    if (client.IsConnected)
                    {
                        client.Authenticate("evt.taskmanager@mail.ru", "bzLG45KL9v1rPdw8xfie");
                        if (client.IsAuthenticated)
                        {
                            await client.SendAsync(mail);
                            _logger.LogInformation("Mail was sent");
                        }
                        else
                        {
                            _logger.LogWarning("Mail service is not authenticated");
                        }
                    }
                    else
                    {
                        _logger.LogWarning("Mail service is not connected");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                }
            };

             
        }
    }
}
