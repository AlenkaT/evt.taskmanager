﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Common.Extensions
{
    public static class ObjectExtentions
    {
        public static string PrintProperties(this object obj)
        {
            var strBuilder= new StringBuilder();
            var objType= obj.GetType();
            strBuilder.AppendLine("Object type: " + objType.Name);
            var propertyInfos= objType.GetProperties(BindingFlags.Public| BindingFlags.Instance);
            foreach (var propertyInfo in propertyInfos)
            {
                strBuilder.AppendLine(propertyInfo.Name + ": "+propertyInfo.GetValue(obj));
            }
            return strBuilder.ToString();
        }
    }
}
