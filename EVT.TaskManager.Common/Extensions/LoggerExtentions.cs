﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EVT.TaskManager.Common.Extensions
{
    public static class LoggerExtentions
    {
        public static void LogMethodResultToFileUTF8(this ILogger logger, object source,  string filePath, string methodName, object? result)
        { 
            if (logger == null || source==null) return;
            if (result == null) result = "null";
            
            var content = "Result of " + source.GetType().Name + "." + methodName + "   " + String.Join(" ;", result);
            try
            {
                logger.LogInformation(content);
                Task.Run(() => File.AppendAllTextAsync(filePath, content, Encoding.UTF8));
            }
            catch (Exception ex)
            {
                logger.LogError("Log information to file: "+filePath+ "was not executed");
            }
        }

        public static void WarnLogMethodResultToFileUTF8(this ILogger logger, object source, string filePath, string methodName, object result)
        {
            if (logger == null || source == null) return;
            if (result == null) result = "null";
            var content = "Warning!: Result of " + source.GetType().Name + "." + methodName + "   " + String.Join(" ;", result);
            try
            {
                logger.LogWarning(content);
                Task.Run(() => File.AppendAllTextAsync(filePath, content, Encoding.UTF8));
            }
            catch (Exception ex)
            {
                logger.LogError("Log warning to file: " + filePath + "was not executed");
            }
        }

        public static void CriticalLogMethodResultToFileUTF8(this ILogger logger, object source, string filePath, string methodName, object result)
        {
            if (logger == null || source == null) return;
            if (result == null) result = "null";
            var content = "Critical result!!!: Result of " + source.GetType().Name + "." + methodName + "   " + String.Join(" ;", result);
            try
            {
                logger.LogCritical(content);
                Task.Run(() => File.AppendAllTextAsync(filePath, content, Encoding.UTF8));
            }
            catch (Exception ex)
            {
                logger.LogCritical("Log critical to file: " + filePath + "was not executed");
            }
        }
    }
}
